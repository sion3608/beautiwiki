package com.crew91.beautiwiki.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import org.apache.ibatis.session.SqlSession;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.crew91.beautiwiki.vo.ReviewVO;

@Component
public class ReviewDAO extends SqlSessionDaoSupport{

	@Autowired
	private SqlSession sqlSession;

	@Autowired
	private DataSource ds;
	public ReviewDAO() throws Exception {
		Context init = new InitialContext();
		ds = (DataSource)init.lookup("java:comp/env/jdbc/OracleDB");
	}
	
	
	
	//가장 최근의 리뷰 4개 정보 select
	public List<ReviewVO> newestReviewList() throws SQLException{
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		List<ReviewVO> newestReviewList = new ArrayList<ReviewVO> ();
		
		try {
			StringBuffer findQuery = new StringBuffer();
			findQuery.append("select rownum, review_no, user_no, cosmetics_no, grade, is_recomm, skin_type, write_time, hit, likes, dislikes, title, img_names, contents ");
			findQuery.append("from (select review_no, user_no, cosmetics_no, grade, is_recomm, skin_type, write_time, hit, likes, dislikes, title, img_names, contents, SYSDATE-write_time as newest ");
			findQuery.append("from tb_review ");
			findQuery.append("order by newest) ");
			findQuery.append("where rownum <= 4");
			
			conn = ds.getConnection();
			pstmt = conn.prepareStatement(findQuery.toString());

			rs = pstmt.executeQuery();

			while (rs.next()) {
				ReviewVO reviewVO = new ReviewVO();
				reviewVO.setReviewNo(rs.getInt("review_no"));
				reviewVO.setUserNo(rs.getInt("user_no"));
				reviewVO.setCosmeticsNo(rs.getInt("cosmetics_no"));
				reviewVO.setGrade(rs.getInt("grade"));
				reviewVO.setIsRecomm(rs.getInt("is_recomm"));
				reviewVO.setSkinType(rs.getString("skin_type"));
				reviewVO.setWriteTime(rs.getTimestamp("write_time"));
				reviewVO.setHit(rs.getInt("hit"));
				reviewVO.setLike(rs.getInt("likes"));
				reviewVO.setDisLike(rs.getInt("dislikes"));
				reviewVO.setTitle(rs.getString("title"));
				reviewVO.setImgNames(rs.getString("img_names"));
				reviewVO.setContents(rs.getString("contents"));
				newestReviewList.add(reviewVO);
			}

		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			if(pstmt != null) {
				pstmt.close();
			}
			if(conn != null) {
				conn.close();
			}
		}
		return newestReviewList;
	}
	//가장 hit수 높은 리뷰 4개 선택
	public List<ReviewVO> highHitReview() throws SQLException{
		Connection conn = null;
		PreparedStatement pstmt = null;
	
		ResultSet rs = null;
		List<ReviewVO> highHitReviewList = new ArrayList<ReviewVO> ();
		try {
			StringBuffer findQuery = new StringBuffer();
			findQuery.append("select rownum, review_no, user_no, cosmetics_no, grade, is_recomm, skin_type, write_time, hit, likes, dislikes, title, img_names, contents ");
			findQuery.append("from (select review_no, user_no, cosmetics_no, grade, is_recomm, skin_type, write_time, hit, likes, dislikes, title, img_names, contents ");
			findQuery.append("from tb_review ");
			findQuery.append("order by hit desc) ");
			findQuery.append("where rownum <= 5");
			conn = ds.getConnection();
			pstmt = conn.prepareStatement(findQuery.toString());

			rs = pstmt.executeQuery();

			while (rs.next()) {
				ReviewVO reviewVO = new ReviewVO();
				reviewVO.setReviewNo(rs.getInt("review_no"));
				reviewVO.setUserNo(rs.getInt("user_no"));
				reviewVO.setCosmeticsNo(rs.getInt("cosmetics_no"));
				reviewVO.setGrade(rs.getInt("grade"));
				reviewVO.setIsRecomm(rs.getInt("is_recomm"));
				reviewVO.setSkinType(rs.getString("skin_type"));
				reviewVO.setWriteTime(rs.getTimestamp("write_time"));
				reviewVO.setHit(rs.getInt("hit"));
				reviewVO.setLike(rs.getInt("likes"));
				reviewVO.setDisLike(rs.getInt("dislikes"));
				reviewVO.setTitle(rs.getString("title"));
				reviewVO.setImgNames(rs.getString("img_names"));
				reviewVO.setContents(rs.getString("contents"));
				highHitReviewList.add(reviewVO);
			}

		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			if(pstmt != null) {
				pstmt.close();
			}
			if(conn != null) {
				conn.close();
			}
		}
		return highHitReviewList;
	}
	public boolean deleteUserAllReview(int user_no) throws SQLException{//회원의 모든 리뷰를 삭제한다.
		Connection conn = null;
		PreparedStatement pstmt = null;
		
		String sql = "delete FROM tb_review where user_no=" + user_no;
				
		try {
			conn = ds.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.executeUpdate(sql);

			
		}catch(Exception e) {
			e.printStackTrace();
			
		}finally {
			if(pstmt != null) {
				pstmt.close();
			}
			if(conn != null) {
				conn.close();
			}
		}
		return true;
	}
	
	//리뷰 디비에 INSERT 하는 함수
	public int insertReview(ReviewVO reviewVO) {
		return getSqlSession().insert("tb_review.insertReview", reviewVO);
	}
	
	/**
	 * 1차 카테고리 아이디와 2차 카테고리 아이디로 리뷰 목록 조회
	 * 
	 * 2차 카테고리 아이디가 0이면 1차 카테고리를 선택한 것이므로 1차 카테고리 전체 목록 조회
	 * 
	 * 2차 카테고리 아이디가 0보다 크면 2차 카테고리를 선택한 것이므로 2차 카테고리 목록 조회
	 * 
	 * @param categoryId
	 * @param subcategoryId
	 * @return
	 * @throws SQLException
	 */
	public List<ReviewVO> getReviewByCategoryIdAndSubcategoryIdPerPage(int categoryId, int subcategoryId, int page, int countPerPage) throws SQLException {
		Map<String, Integer> params = new HashMap<String, Integer>();
		params.put("categoryId", categoryId);
		params.put("subcategoryId", subcategoryId);
		params.put("start", ((page - 1) * countPerPage));
		params.put("end", (page * countPerPage));
		
		return getSqlSession().selectList("tb_review.selectReviewByCategoryIdAndSubcategoryIdPerPage", params);
	}
	
	/**
	 * 브랜드 아이디로 리뷰 목록 조회
	 * 
	 * 브랜드 아이디가 0이면 조회하지 않는다. (너무 목록이 많아 조회 X)
	 * 
	 * @param brandId
	 * @return
	 */
	public List<ReviewVO> getReviewByBrandIdPerPage(int brandId, int page, int countPerPage) throws SQLException {
		if (brandId == 0) {
			return null;
		}
		
		Map<String, Integer> params = new HashMap<String, Integer>();
		params.put("brandId", brandId);
		params.put("start", ((page - 1) * countPerPage));
		params.put("end", (page * countPerPage));

		return getSqlSession().selectList("tb_review.selectReviewByBrandIdPerPage", brandId);
	}

	/*
	 * params : reviewNo(리뷰번호)
	 * 목적 : reviewList에서 review클릭시 reviewNo로 해당 리뷰 가지고 오는 메소드
	 */
	public ReviewVO selectReviewByReviewNo(int reviewNo) throws SQLException{
		
		return getSqlSession().selectOne("tb_review.selectReviewByReviewNo", reviewNo);
	}

	
	/*
	 * 목적 : 해당 리뷰를 클릭했을 시에 조회수를 증가시켜주는 함수
	 */
	public int updateHitNumber(int reviewNo) throws SQLException {
		return getSqlSession().update("tb_review.updateHitNumber", reviewNo);
	}


	/*
	 * 목적 : 리뷰 추천눌럿을 때 추천수 증가해주는 함수
	 */
	public int updateLikeNumber(int reviewNo)throws SQLException{
		return getSqlSession().update("tb_review.updateLikeNumber", reviewNo);
	}
	
	public int updateDisLikeNumber(int reviewNo)throws SQLException {
		return getSqlSession().update("tb_review.updateLikeNumber", reviewNo);
	}


	public List<ReviewVO> selectReviewBycosId(int cosmeticsNo) throws SQLException{
		return getSqlSession().selectList("tb_review.selectReviewBycosId", cosmeticsNo);
	}
}
