package com.crew91.beautiwiki.vo;

import org.springframework.stereotype.Component;

@Component
public class PreferVO {
	private int preferId;
	private int userId;
	private int brandId;
	
	public PreferVO() {}
	public PreferVO(int userId, int brandId) {
		this.userId = userId;
		this.brandId = brandId;
	}
	public int getPreferId() {
		return preferId;
	}
	public int getUserId() {
		return userId;
	}
	public int getBrandId() {
		return brandId;
	}
	public void setPreferId(int preferId) {
		this.preferId = preferId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public void setBrandId(int brandId) {
		this.brandId = brandId;
	}
	public String toString() {
		return "PreferVO [preferId=" + preferId + ", userId=" + userId + ", brandId=" + brandId + "]";
	}
}
