/*
 * 작성자 : 김 시온
 * 일자 : 2014.11.10일
 * 목적 : Review와 관련된 요청처리하는 클래스
 * 수정사항 : 
 */
package com.crew91.beautiwiki.controller;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.crew91.beautiwiki.bo.SearchBO;
import com.crew91.beautiwiki.dao.SearchDAO;
import com.crew91.beautiwiki.vo.CosmeticVO;
import com.crew91.beautiwiki.vo.ReviewVO;
import com.crew91.beautiwiki.vo.SubCategoryVO;
import com.google.gson.Gson;

@Controller
public class SearchController {
	
	@Autowired
	private SearchDAO searchDAO;
	@Autowired
	private SearchBO searchBO;
	
	//브랜드 아이디와 1차 카테고리아이디로 서브카테고리 가져오는 메소드
	@RequestMapping(value="/seachSubcat.do", method=RequestMethod.GET) 
	public String seachSubcat(Model model, @RequestParam("brandId") int brandId
															, @RequestParam("catId")int catId)	{
		
		List<SubCategoryVO> subCatList = null;
		JSONObject json = new JSONObject();
		Gson gson = new Gson();
		try {
			subCatList = searchDAO.selectSubcatByBrandCat(brandId, catId);
			json.put("subCatList", gson.toJson(subCatList));
			json.put("isSuccess", true);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			json.put("isSuccess", false);
		}
		model.addAttribute("json", json);
		return "json";
	}
	
	//브랜드아이디와 2차카테고리 아이디로 화장품가져오는 메소드
	@RequestMapping(value="seachCosmetic.do", method=RequestMethod.GET)
	public String seachCosmetic(Model model, @RequestParam("brandId") int brandId
															, @RequestParam("subcatId")int subcatId){
		
		List<CosmeticVO> cosList = null;
		JSONObject json = new JSONObject();
		Gson gson = new Gson();
		try {
			cosList = searchDAO.selectCosByBrandSubcat(brandId, subcatId);
			for (CosmeticVO cosmeticVO : cosList) {
				cosmeticVO.setImgUrl("/editor/cosmetics/" + StringUtils.trimWhitespace(cosmeticVO.getName()) + ".JPG");
			}
			
			json.put("cosList", gson.toJson(cosList));
			json.put("isSuccess", true);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			json.put("isSuccess", false);
		}
		model.addAttribute("json", json);
		return "json";
	}
	
	@RequestMapping(value="searchKeywordForReview.do")
	public String searchKeywordForReview(Model model, @RequestParam("searchKeyword") String searchKeyword) {
	
		List<Map<String, Object>> processedReviewList = null;
		
		try {
			processedReviewList = searchBO.searchKeywordForReview(searchKeyword);
			model.addAttribute("processedReviewList", processedReviewList);
			// 네비게이션에 필요한 정보 조회
			model.addAttribute("navigationInfo", searchBO.getNavigationInfo());
			model.addAttribute("flagMsg", "search");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "searchResult";
	}
	

}