<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
<title>Insert title here</title>
</head>
<body>
<script>
function updateUser1(){
	f.action = "updateUser1.do";
	f.submit();
}
function logout(){
	f.action = "logout.do";
	f.submit();
}

</script>
<c:set var = "userSession" scope = "session" value = "${user}"/>
<form name = "f" method = post>
<input type = hidden name = "upNo" value = "1"> <%--upNo에 따라 컨트롤러 작업이 달라진다. 1일시 다음 페이지로 이동. --%>
<table>
	<tr>
		<td colspan = 2>${userSession.userId}님 반갑습니다. </td>
	</tr>
	<tr>
		<input type = hidden name = "userId" value = "${user.userId}">
		<td><input type = button value = "회원정보 수정" onClick="updateUser1()"></td>
		<td><input type = button value = "로그아웃" onClick="logout()" ></td>
	</tr>
</table>
</form>
<form action = "home.do">
<input type = hidden name = "userId" value = "${user.userId}">
<input type = submit value = "메인페이지가기">
</form>
</body>
</html>