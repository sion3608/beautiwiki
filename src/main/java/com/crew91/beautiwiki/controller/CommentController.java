package com.crew91.beautiwiki.controller;

import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.crew91.beautiwiki.bo.CommentBO;
import com.crew91.beautiwiki.bo.ReviewBO;
import com.crew91.beautiwiki.bo.SearchBO;
import com.crew91.beautiwiki.vo.CommentVO;
import com.crew91.beautiwiki.vo.ReviewVO;


@Controller
public class CommentController {
	@Autowired
	private CommentBO commentBO;

	@Autowired
	private ReviewBO reviewBO;
	@Autowired
	SearchBO searchBO;
	
	/*댓긍르 insert reviewNO는 리뷰를 데려오기 위한것, userNo와 contents는 댓글을 달기 위한것*/
	@RequestMapping(value="/insertComment.do")
	public String insertComment(Model model, @RequestParam("reviewNo")int reviewNo, @RequestParam("userNo") int userNo, @RequestParam("comment")String contents) {
		commentBO.insertComment(reviewNo, userNo, contents);//코멘트 삽입작업
		
		try {
			List<CommentVO> commentlist = commentBO.getCommentList(reviewNo); //리뷰에 달린 커맨트를 출력
			ReviewVO reviewVO = reviewBO.selectReviewByReviewNo(reviewNo);
			model.addAttribute("review", reviewVO);
			model.addAttribute("commentlist", commentlist);
			// 네비게이션에 필요한 정보 조회
			model.addAttribute("navigationInfo", searchBO.getNavigationInfo());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} // 리뷰를 얻어오는
		
		return "resultReview"; //리뷰보는 페이지로가야함
	}
	
	
	/*댓글수정페이지로 이동하기 위한 *
	 * 수정해야할 부분-> goReview로 가는 거 reviewController에서 처리
	 * /
	/*댓글을 수정하기 위한 */
	@RequestMapping(value="/commentFix2.do")
	public String commentFix(Model model, Model model2, @RequestParam("reviewNo")int reviewNo, @RequestParam("commentNo")int commentNo, @RequestParam("contents")String contents) {
		//ReviewVO review = reviewBO.goReview(reviewNo); // 리뷰를 얻어오는
		
		commentBO.commentFix(commentNo, contents); // 댓글을 수정한다. commentNo가 있야 한다.
		
		try {
			List<CommentVO> commentlist = commentBO.getCommentList(reviewNo); //리뷰에 달린 커맨트를 출력
			ReviewVO reviewVO = reviewBO.selectReviewByReviewNo(reviewNo);
			model.addAttribute("review", reviewVO);
			model.addAttribute("commentlist", commentlist);
			// 네비게이션에 필요한 정보 조회
			model.addAttribute("navigationInfo", searchBO.getNavigationInfo());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} // 리뷰를 얻어오는

		return "resultReview";//리뷰보는 페이지로가야함
	}
	
	//댓글수정페이지로 이동하기 위한 /
	@RequestMapping(value="/commentFix.do")
	public String commentFix(Model model, @RequestParam("reviewNo")int reviewNo, @RequestParam("commentNo")int commentNo, @RequestParam("userNo") int userNo, @RequestParam("contents")String contents) {
		
		try {
			List<CommentVO> commentlist = commentBO.getCommentList(reviewNo); //리뷰에 달린 커맨트를 출력
			ReviewVO reviewVO = reviewBO.selectReviewByReviewNo(reviewNo);
			model.addAttribute("review", reviewVO);
			model.addAttribute("commentlist", commentlist);
			model.addAttribute("commentNo", commentNo);
			model.addAttribute("is", 1);
			// 네비게이션에 필요한 정보 조회
			model.addAttribute("navigationInfo", searchBO.getNavigationInfo());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} // 리뷰를 얻어오는

	    return "resultReview";
	}
	
	@RequestMapping(value="/deleteComment.do")
	public String commentFix(Model model, Model model2, @RequestParam("reviewNo")int reviewNo, @RequestParam("commentNo")int commentNo) {
		//ReviewVO review = reviewBO.goReview(reviewNo); // 리뷰를 얻어오는
		
		commentBO.deleteComment(commentNo);
		
		try {
			List<CommentVO> commentlist = commentBO.getCommentList(reviewNo); //리뷰에 달린 커맨트를 출력
			ReviewVO reviewVO = reviewBO.selectReviewByReviewNo(reviewNo);
			model.addAttribute("review", reviewVO);
			model.addAttribute("commentlist", commentlist);
			// 네비게이션에 필요한 정보 조회
			model.addAttribute("navigationInfo", searchBO.getNavigationInfo());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} // 리뷰를 얻어오는

		return "resultReview";//리뷰보는 페이지로가야함
	}
	
	@RequestMapping(value="/go.do")
	public String go() {
		return "tmp_comment";
	}
	
}
