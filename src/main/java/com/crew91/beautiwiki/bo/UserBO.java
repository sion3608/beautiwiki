package com.crew91.beautiwiki.bo;
import java.sql.SQLException;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.crew91.beautiwiki.dao.BrandDAO;
import com.crew91.beautiwiki.dao.CommentDAO;
import com.crew91.beautiwiki.dao.PreferDAO;
import com.crew91.beautiwiki.dao.RecommAgeDAO;
import com.crew91.beautiwiki.dao.ReviewDAO;
import com.crew91.beautiwiki.dao.UserDAO;
import com.crew91.beautiwiki.vo.BrandVO;
import com.crew91.beautiwiki.vo.PreferVO;
import com.crew91.beautiwiki.vo.UserVO;

@Component
public class UserBO {
	@Autowired
	private UserDAO userDAO;
	@Autowired
	private PreferDAO preferDAO;
	
	@Autowired
	private BrandDAO brandDAO;
	
	@Autowired
	private CommentDAO commentDAO;
	
	@Autowired
	private RecommAgeDAO recommAgeDAO;
	
	@Autowired
	private ReviewDAO reviewDAO;
	
	@Autowired
	private UserVO userVO = null;
	
	public UserVO makeUser(String userId, String password, String password2, String userName, int age, int sex, String skinType){
		UserVO userInfo = null;
		userInfo = new UserVO(userId, password, sex, age, skinType, userName);
		System.out.println(userInfo.toString());
		return userInfo;
	}
	
	public void signUpService(UserVO userInfo, String[] brandList) throws SQLException{
		if (userInfo != null) {
			int userNO = userDAO.insert(userInfo);
			System.out.println("userNO: " + userNO);
			if (userNO != -1) {
				List<PreferVO> preferVOlist = new ArrayList<PreferVO> ();
				for (int i = 0; i < brandList.length; i++) {
					PreferVO preferVO = new PreferVO(userNO, Integer.parseInt(brandList[i]));
					preferVOlist.add(preferVO);
				}
				for (PreferVO preferVo : preferVOlist) {
					System.out.println(preferVo.toString());
				}
				int ox = preferDAO.insertPrefer(preferVOlist);
				if (ox == 0) {
					System.out.println("prefer insert에 실패했습니다.");
				}
			}
		}
	}
	
	public UserVO login(String userId, String password) {
		try {
			userVO = userDAO.login(userId);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return userVO;
	}

	public UserVO updateUser1(String userId) {
		try {
			userVO = userDAO.getUserInfo(userId);
		} catch (SQLException e) {
			e.printStackTrace();
		} //회원정보를 가져오는 작업
		return userVO;
	}

	public void updateUser2(UserVO user, int[] prefer) {
		try {
			userDAO.updateUser(user);
			preferDAO.deleteUserPrefer(user.getUserNo(), prefer);
			preferDAO.insertPrefer(user.getUserNo(), prefer);
		} catch (SQLException e) {
			e.printStackTrace();
		}		
	}
	public boolean deleteUser(int userNo) throws SQLException {
		commentDAO.deleteUserAllComment(userNo);
		recommAgeDAO.deleteUserAllRecomm_age(userNo);
		preferDAO.deletePrefer(userNo);	
		reviewDAO.deleteUserAllReview(userNo);
		boolean tf = true;
		tf = userDAO.deleteUser(userNo);
		return tf;
	}
	
	public List<BrandVO> getPrefer(int userNo) {
		try {
			return preferDAO.getPrefer(userNo);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} //회원정보를 가져오는 작업
	}
	
	public List<BrandVO> getBrandList() {
		try {
			return brandDAO.selectAllBrand();
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} //회원정보를 가져오는 작업
	}
}

