package com.crew91.beautiwiki.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.crew91.beautiwiki.vo.CommentVO;
import com.crew91.beautiwiki.vo.UserVO;

@Component
public class CommentDAO {

	@Autowired
	private SqlSession sqlSession; //���ǹ���
	
	Connection conn = null;
	PreparedStatement pstmt = null;

	String jdbc_driver = "oracle.jdbc.OracleDriver";
	String jdbc_url = "jdbc:oracle:thin:@202.20.119.117:1521:orcl";
	
	public void connect() {
		try {
			Class.forName(jdbc_driver);
			conn = DriverManager.getConnection(jdbc_url,"crew91","beautiwiki");
		} catch (Exception e) {
			System.out.print("��� ����");
		}
	}
	
	public void disconnect() {
		if(pstmt != null) {
			try {
				pstmt.close();
			} catch(SQLException e) {
				e.printStackTrace();
			}
		}
		if(conn != null) {
			try {
				conn.close();
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public boolean deleteUserAllComment(int user_no) {//ȸ���� ��� ����� �����Ѵ�.
		connect();
		
		String sql = "delete FROM tb_comment where user_id=" + user_no;
				
		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.executeUpdate(sql);

			
		}catch(Exception e) {
			e.printStackTrace();
			
		}finally {
			disconnect();
		}
		return true;
	}
	
	public boolean insertComment(int reviewNo, int userNo, String comment) {
		connect();
		
		String sql = "insert into tb_comment values(sq_comment.nextval, ?, ?, 0, ?, sysdate)";
		
		try {
			
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, userNo);
			pstmt.setInt(2, reviewNo);
			pstmt.setString(3, comment);
			pstmt.executeUpdate();
		
		} catch(SQLException e) {
			e.printStackTrace();
			return false;
		}
		finally {
			disconnect();
		}
		return true;
	}
	
	public List<CommentVO> getCommentList(int review_no) {
		PreparedStatement pstmt2 = null;
		connect();
	
		ResultSet rs = null;
		List<CommentVO> alist = new ArrayList<CommentVO>();
		
		try {
			StringBuffer findQuery = new StringBuffer();
			findQuery.append("select c.comment_no as commentNo, c.user_id as userNo, c.review_no as reviewNo, c.contents as contents, c.comment_date as commentDate, u.user_id as userId from tb_comment c, tb_user u ");
			findQuery.append("where c.user_id = u.user_no and c.review_no = ?");

			pstmt2 = conn.prepareStatement(findQuery.toString());
			pstmt2.setInt(1, review_no);
			rs = pstmt2.executeQuery();
			System.out.print("실행");
			System.out.println(findQuery.toString());
			while(rs.next()) {
				System.out.println("1살향1");
				CommentVO comment = new CommentVO();
				comment.setComment_no(rs.getInt("commentNo"));
				comment.setUser_id(rs.getInt("userNo"));
				comment.setReview_no(rs.getInt("reviewNo"));
				comment.setContents(rs.getString("contents"));
				comment.setDate(rs.getString("commentDate"));
				comment.setUserId(rs.getString("userId"));
				alist.add(comment);
			}
			
		}catch(Exception e) {
			
		}finally {
			disconnect();
		}
		return alist;
	}
	

	public boolean commentFix(int commentNo, String contents) {
		connect();
		
		try {
			String sql = "UPDATE tb_comment set contents = '" + contents + "', comment_date = sysdate where comment_no = " + commentNo;
			pstmt = conn.prepareStatement(sql);
			pstmt.executeUpdate();
			
		}catch(Exception e) {
			e.printStackTrace();
			return false;
			
		}finally {
			disconnect();
		}
		
		return true;
	}
	
	public boolean deleteComment(int commentNo) {
		connect();
				
		String sql = "delete from tb_comment where comment_no=" + commentNo;				
		
		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.executeUpdate(sql);
				
			}catch(Exception e) {
				e.printStackTrace();
				return false;
			}finally {
				disconnect();
			}
			return true;
		}
}
