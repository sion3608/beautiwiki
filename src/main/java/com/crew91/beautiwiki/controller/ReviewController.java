/*
 * 작성자 : 김 시온
 * 일자 : 2014.11.10일
 * 목적 : Review와 관련된 요청처리하는 클래스
 * 수정사항 : 
 */
package com.crew91.beautiwiki.controller;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.crew91.beautiwiki.bo.CommentBO;
import com.crew91.beautiwiki.bo.MyReviewBO;
import com.crew91.beautiwiki.bo.ReviewBO;
import com.crew91.beautiwiki.bo.SearchBO;
import com.crew91.beautiwiki.dao.MyReviewDAO;
import com.crew91.beautiwiki.dao.SearchDAO;
import com.crew91.beautiwiki.vo.BrandVO;
import com.crew91.beautiwiki.vo.CategoryVO;
import com.crew91.beautiwiki.vo.CommentVO;
import com.crew91.beautiwiki.vo.MyReviewVO;
import com.crew91.beautiwiki.vo.ReviewVO;
import com.google.gson.Gson;

@Controller
@SessionAttributes({"userNo", "userId"})
public class ReviewController {
	
	private static final int COUNT_PER_PAGE = 20;
	
	@Autowired
	private SearchDAO searchDAO;
	@Autowired
	private ReviewBO reviewBO;
	@Autowired
	SearchBO searchBO;
	@Autowired
	private CommentBO commentBO;
	@Autowired
	private MyReviewBO myReviewBO;
	
	//review를 쓸 수 있는 페이지로 이동하는 메소드
	@RequestMapping(value="/createReview.do")
	public String showReviewEditor(Model model) {
		List<BrandVO> brandList = null;
		List<CategoryVO> catList = null;
		try {
			brandList = searchDAO.selectAllBrand();
			catList = searchDAO.selectAllCategory();
			model.addAttribute("allCatList", catList);
			model.addAttribute("allBrandList", brandList);
			// 네비게이션에 필요한 정보 조회
			model.addAttribute("navigationInfo", searchBO.getNavigationInfo());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 return "writeReview";
	}
	
	@RequestMapping(value="/saveReview.do")
	public String saveReview(Model model,  @RequestParam("textAreaContent")String textAreaContent,  
			@RequestParam("cosId") int cosId,@RequestParam("title") String title, @RequestParam("skinType") String skinType, 
			@RequestParam("isRecomm") int isRecomm, @RequestParam("grade") int grade, @RequestParam("imgNames") String imgNames, 
			HttpServletRequest request, @ModelAttribute("userNo") int userNo) {
		
		int resReviewNo = 0;
		ReviewVO reviewVO = null;
		
		String[] generation = request.getParameterValues("generation");
		try {
			resReviewNo = reviewBO.createReview(userNo, textAreaContent, cosId, title, skinType, isRecomm, grade, generation, imgNames);
			reviewVO = reviewBO.selectReviewByReviewNo(resReviewNo);
			model.addAttribute("review", reviewVO);
			// 네비게이션에 필요한 정보 조회
			model.addAttribute("navigationInfo", searchBO.getNavigationInfo());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "resultReview";
	}
	
	/**
	 * 리뷰 목록 페이지
	 * 
	 * 일반 카테고리 탭으로 접근시 'd1Id : 1차 카테고리 아이디, d2Id : 2차 카테고리 아이디'
	 * 
	 * 브랜드 탭으로 접근시 'd1Id : 0, d2Id : 브랜드 아이디'
	 * 
	 * @param model
	 * @param d1Id
	 * @param d2Id
	 * @return
	 */
	@RequestMapping(value="/reviewList.do")
	public String reviewList(Model model, @RequestParam("d1Id") int d1Id,
			@RequestParam("d2Id") int d2Id, @RequestParam(value="page", defaultValue="1") int page,
			@RequestParam(value="isResponseJson", defaultValue="false") boolean isResponseJson) {
		
		JSONObject json = new JSONObject();
		Gson gson = new Gson();
		
		if (d1Id < 0 || d2Id < 0 || page < 1) {
			if (isResponseJson) {
				json.put("isSuccess", false);
				json.put("errorMessage", "정상적인 파라미터가 아닙니다.");
				model.addAttribute("json", json.toString());
				return "json";
			} else {
				// 에러페이지로 리다이렉트
			}
		}
		
		if (!isResponseJson) {
			// 네비게이션에 필요한 정보 조회
			model.addAttribute("navigationInfo", searchBO.getNavigationInfo());
		}
		
		// 리뷰 목록 조회
		List<Map<String, Object>> processedReviewList = null;
		
		boolean selectBrandTab = (d1Id == 0);
		if (selectBrandTab) {
			System.out.println("brandTab");
			
			// d1Id가 0인 경우는 브랜드 탭을 선택했을 경우
			// d2Id는 brandId를 뜻한다.
			// d2Id, 즉 brandId가 0이면 브랜드 전체를 뜻하는데,
			// 브랜드 목록을 가져오기에는 너무 많아서 null을 리턴해준다. (UI에서 막아야함)
			processedReviewList = reviewBO.getProcessedReviewListByBrandId(d2Id, page, COUNT_PER_PAGE);
		} else {
			processedReviewList = reviewBO.getProcessedReviewListByCategoryIdAndSubcategoryId(d1Id, d2Id, page, COUNT_PER_PAGE);
		}
		
		if (CollectionUtils.isNotEmpty(processedReviewList)) {
			model.addAttribute("processedReviewList", processedReviewList);
		}
		
		if (isResponseJson) {
			json.put("isSuccess", true);
			json.put("processedReviewList", gson.toJson(processedReviewList));
			model.addAttribute("json", json.toString());
			return "json";
		}
		
		model.addAttribute("d1Id", d1Id);
		model.addAttribute("d2Id", d2Id);
		
		return "reviewList";
	}
	
	@RequestMapping(value="/showReview.do")
	public String reviewList(Model model, @RequestParam("reviewNo") int reviewNo) {
		
		ReviewVO reviewVO = null;
		List<CommentVO> commentlist = null;
		try {
			reviewVO = reviewBO.selectReviewByReviewNo(reviewNo);
			reviewBO.updateHitNumber(reviewNo);
			
			commentlist = commentBO.getCommentList(reviewNo);
			model.addAttribute("review", reviewVO);
			// 네비게이션에 필요한 정보 조회
			model.addAttribute("navigationInfo", searchBO.getNavigationInfo());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return "resultReview";
	}
	
	@RequestMapping(value="/addMyReview.do")
	public String addMyReview(Model model, @RequestParam("reviewNo") int reviewNo, @ModelAttribute("userNo") int userNo) {
		String resultMsg;
		JSONObject json = new JSONObject();
		Gson gson = new Gson();
		System.out.println(reviewNo + "," + userNo);
		try {
			resultMsg = myReviewBO.validateMyReview(reviewNo, userNo);
			if(resultMsg.equals("succInputMyReview")) {
				json.put("isSuccess", true);
				json.put("successMessage", "리뷰보관함에 담겨졌습니다.");
			} 
			else{
				json.put("isSuccess", false);
				if(resultMsg.equals("failInputMyReview")) {
					json.put("errorMessage", "리뷰보관에 실패했습니다.");
				} else {
					json.put("errorMessage", "이미 리뷰보관함에 있습니다.");
				}
			}
			
			model.addAttribute("json", json.toString());
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "json";
	}
	
	@RequestMapping(value="/showMyReview.do")
	public String showMyReview(Model model, @ModelAttribute("userNo")int userNo)
	{
		List<Map<String, Object>> processedReviewList = null;
		try {
			processedReviewList = searchBO.selectMyReviewByUserNo(userNo);
			model.addAttribute("processedReviewList", processedReviewList);
			
			// 네비게이션에 필요한 정보 조회
			model.addAttribute("navigationInfo", searchBO.getNavigationInfo());
			model.addAttribute("flagMsg", "myReview");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "searchResult";
	}
	
	@RequestMapping(value="/recommReview.do")
	public String recommReview(Model model, @RequestParam("reviewNo")int reviewNo){
		int result;
		JSONObject json = new JSONObject();
		Gson gson = new Gson();
		try {
			result = reviewBO.updateLikeNumber(reviewNo);
			if(result > 0) {
				json.put("isSuccess", true);
				json.put("successMessage", "추천하셨습니다.");
			} 
			else{
				json.put("isSuccess", false);
				json.put("errorMessage", "리뷰추천에 실패했습니다.");
			}
			
			model.addAttribute("json", json.toString());
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "json";
	}
	
	@RequestMapping(value="/notRecommReview.do")
	public String notRecommReview(Model model, @RequestParam("reviewNo")int reviewNo){
		int result;
		JSONObject json = new JSONObject();
		Gson gson = new Gson();
		try {
			result = reviewBO.updateDisLikeNumber(reviewNo);
			if(result > 0) {
				json.put("isSuccess", true);
				json.put("successMessage", "비추천하셨습니다.");
			} 
			else{
				json.put("isSuccess", false);
				json.put("errorMessage", "리뷰추천에 실패했습니다.");
			}
			
			model.addAttribute("json", json.toString());
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "json";
	}
	
	@RequestMapping(value="showCosmetics.do")
	public String showCosmetics(Model model, @RequestParam("cosmeticsNo") int cosmeticsNo) {
		
		List<Map<String, Object>> processedReviewList = null;
		
		try {
			processedReviewList = reviewBO.selectReviewBycosId(cosmeticsNo);
			model.addAttribute("processedReviewList", processedReviewList);
			// 네비게이션에 필요한 정보 조회
			model.addAttribute("navigationInfo", searchBO.getNavigationInfo());
			model.addAttribute("flagMsg", "search");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "searchResult";
	}
}