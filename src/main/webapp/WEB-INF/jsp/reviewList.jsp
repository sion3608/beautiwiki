<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<c:set var="currentPageCss" value="reviewList.css" scope="request" />
<jsp:include page="/WEB-INF/jsp/common/header.jsp" />

<div class="section group">
	<div>
		<div class="col" style="width:14.5%;">
			<div class="side_menu">
				<c:if test="${not empty navigationInfo && not empty navigationInfo.categoryInfoList}">
					<div class="side_menu_d_0_id_area _sideMenuOfCategoryArea">
						<c:forEach var="categoryInfo" items="${navigationInfo.categoryInfoList}" varStatus="sequence">
							<div class="side_menu_d_1_id_area">
								<div class="_sideMenuOfCategory _params('${categoryInfo.categoryId},0')">
									<span class="_nameText<c:if test="${categoryInfo.categoryId eq d1Id}"> on</c:if>">${categoryInfo.categoryName}</span>
								</div>
								<div class="_sideMenuOfSubcategoryArea side_menu_d_2_id_area" style="<c:if test="${categoryInfo.categoryId ne d1Id}">display:none;</c:if>">
									<c:forEach var="subcategoryInfo" items="${categoryInfo.subCategoryList}" varStatus="subSequence">
										<div class="side_menu_d_2_id">
											<div class="_sideMenuOfSubcategory _params('${subcategoryInfo.categoryId},${subcategoryInfo.subcategoryId }')">
												<span>${subcategoryInfo.name}</span>
											</div>
										</div>
									</c:forEach>
								</div>
							</div>
						</c:forEach>
					</div>
				</c:if>
				<c:if test="${not empty navigationInfo && not empty navigationInfo.brandList}">
					<div class="side_menu_d_0_id_area _sideMenuOfBrandArea">
						<c:forEach var="brand" items="${navigationInfo.brandList}" varStatus="sequence">
							<div class="side_menu_d_1_id_area">
								<div class="_sideMenuOfBrand _params('0,${brand.brandId}')">
									<span class="_nameText">${brand.name}</span>
								</div>
							</div>
						</c:forEach>
					</div>
				</c:if>
			</div>
		</div>
		<div class="col" style="width:82.5%">
			<div class="review_container">
				<ul id="tiles">
					<c:if test="${not empty processedReviewList}">
						<c:forEach var="processedReview" items="${processedReviewList}" varStatus="sequence">
							<li<c:if test="${not empty processedReview.review}"> class="_params('${processedReview.review.reviewNo}')"</c:if>>
								<c:if test="${not empty processedReview.review}">
									<div class="_clickable_area">
										<c:if test="${not empty processedReview.imgUrlList}">
											<c:forEach var="imgUrl" items="${processedReview.imgUrlList}" varStatus="sequence">
												<c:if test="${sequence.count eq 1}">
													<div class="review_img_area">
														<img class="review_img" src="${imgUrl}" width="220">
													</div>
												</c:if>
											</c:forEach>
										</c:if>
										
										<div class="review_content_area">
											<div class="review_recomm_area">
												<c:if test="${processedReview.review.isRecomm eq 1}">
													<img class="recommend_img" src="/static/img/smile.png" width="20" height="20">
												</c:if>
												<c:if test="${processedReview.review.isRecomm eq 0}">
													<img class="recommend_img" src="/static/img/sad.png" width="20" height="20">
												</c:if>
											</div>
											
											<div class="review_title_area">
												${processedReview.review.title}
											</div>
											
											<div class="review_hit_and_writedate_area">
												조회수 <span class="point">${processedReview.review.hit}</span> <span class="division">|</span> <fmt:formatDate value="${processedReview.review.writeTime}" pattern="yyyy.MM.dd"></fmt:formatDate>
											</div>
										</div>
										
										<div class="review_grade_area">
											<c:forEach var="grade" begin="1" end="5" step="1">
												<c:set var="tempGrade" value="${6 - grade}"/>
												<c:if test="${processedReview.review.grade <= tempGrade}">
													<img style="float:left" class="grade_img" src="/static/img/star_on.png" width="15" height="15">
												</c:if>
												<c:if test="${processedReview.review.grade > tempGrade}">
													<img style="float:left" class="grade_img" src="/static/img/star_off.png" width="15" height="15">
												</c:if>
											</c:forEach>
										</div>
									</div>
											
									<div class="review_like_and_dislike">
										<table style="width:100%">
											<tr>
												<td>
													좋아요 <span class="point">${processedReview.review.like}</span>	
												</td>
												<td>
													싫어요 <span class="point">${processedReview.review.disLike}</span>
												</td>
												<td onClick="window.open('http://www.facebook.com/sharer/sharer.php?u='+location.href)">
													공유하기
												</td>
											</tr>
										</table>
									</div>
								</c:if>
							</li>
						</c:forEach>
					</c:if>
					<c:if test="${empty processedReviewList}">
						<div style="width:100%;text-align:center;padding-top:100px;font-size:20px;font-weight:bold">리뷰 목록이 존재하지 않습니다.<div>
					</c:if>
				</ul>
			</div>
		</div>
	</div>
</div>

<script type="text/template" id="empty_review_template">
	<div style="width:100%;text-align:center;padding-top:100px;font-size:20px;font-weight:bold">리뷰 목록이 존재하지 않습니다.<div>
</script>

<script type="text/template" id="review_template">
	<li class="_params('{=reviewNo}')">
	<div class="_clickable_area">	
		<div class="review_img_area">
			<img class="review_img" src="{=imgUrl}" width="220">
		</div>

		<div class="review_content_area">
			<div class="review_recomm_area">
				{if isRecomm}<img class="recommend_img" src="/static/img/smile.png" width="20" height="20">{else}<img class="recommend_img" src="/static/img/sad.png" width="20" height="20">{/if}
			</div>
			
			<div class="review_title_area">
				{=title}
			</div>
			
			<div class="review_hit_and_writedate_area">
				조회수 <span class="point">{=hit}</span> <span class="division">|</span> {=writeTime}
			</div>
		</div>

		<div class="review_grade_area">
			{if grade == 0}
				<img style="float:left" class="grade_img" src="/static/img/star_off.png" width="15" height="15">
				<img style="float:left" class="grade_img" src="/static/img/star_off.png" width="15" height="15">
				<img style="float:left" class="grade_img" src="/static/img/star_off.png" width="15" height="15">
				<img style="float:left" class="grade_img" src="/static/img/star_off.png" width="15" height="15">
				<img style="float:left" class="grade_img" src="/static/img/star_off.png" width="15" height="15">
			{elseif grade == 1}
				<img style="float:left" class="grade_img" src="/static/img/star_on.png" width="15" height="15">
				<img style="float:left" class="grade_img" src="/static/img/star_off.png" width="15" height="15">
				<img style="float:left" class="grade_img" src="/static/img/star_off.png" width="15" height="15">
				<img style="float:left" class="grade_img" src="/static/img/star_off.png" width="15" height="15">
				<img style="float:left" class="grade_img" src="/static/img/star_off.png" width="15" height="15">
			{elseif grade == 2}
				<img style="float:left" class="grade_img" src="/static/img/star_on.png" width="15" height="15">
				<img style="float:left" class="grade_img" src="/static/img/star_on.png" width="15" height="15">
				<img style="float:left" class="grade_img" src="/static/img/star_off.png" width="15" height="15">
				<img style="float:left" class="grade_img" src="/static/img/star_off.png" width="15" height="15">
				<img style="float:left" class="grade_img" src="/static/img/star_off.png" width="15" height="15">
			{elseif grade == 3}
				<img style="float:left" class="grade_img" src="/static/img/star_on.png" width="15" height="15">
				<img style="float:left" class="grade_img" src="/static/img/star_on.png" width="15" height="15">
				<img style="float:left" class="grade_img" src="/static/img/star_on.png" width="15" height="15">
				<img style="float:left" class="grade_img" src="/static/img/star_off.png" width="15" height="15">
				<img style="float:left" class="grade_img" src="/static/img/star_off.png" width="15" height="15">
			{elseif grade == 4}
				<img style="float:left" class="grade_img" src="/static/img/star_on.png" width="15" height="15">
				<img style="float:left" class="grade_img" src="/static/img/star_on.png" width="15" height="15">
				<img style="float:left" class="grade_img" src="/static/img/star_on.png" width="15" height="15">
				<img style="float:left" class="grade_img" src="/static/img/star_on.png" width="15" height="15">
				<img style="float:left" class="grade_img" src="/static/img/star_off.png" width="15" height="15">
			{else}
				<img style="float:left" class="grade_img" src="/static/img/star_on.png" width="15" height="15">
				<img style="float:left" class="grade_img" src="/static/img/star_on.png" width="15" height="15">
				<img style="float:left" class="grade_img" src="/static/img/star_on.png" width="15" height="15">
				<img style="float:left" class="grade_img" src="/static/img/star_on.png" width="15" height="15">
				<img style="float:left" class="grade_img" src="/static/img/star_on.png" width="15" height="15">
			{/if}
		</div>
	</div>			
	<div class="review_like_and_dislike">
		<table style="width:100%">
			<tr>
				<td>
					좋아요 <span class="point">{=like}</span>	
				</td>
				<td>
					싫어요 <span class="point">{=dislike}</span>
				</td>
				<td>
					공유하기
				</td>
			</tr>
		</table>
	</div>
	</li>
</script>

<jsp:include page="/WEB-INF/jsp/common/footer.jsp"/>
<script src="${JS_DIR}/jquery-1.10.1.js"></script>
<script src="${JS_DIR}/jquery.wookmark.js"></script>
<script type="text/javascript">
var ReviewList = jindo.$Class({
	_oElement : null,
	_wElement : null,
	_oTemplate : null,
	_oEvent : null,
	
	_nCategoryId : 1,
	_nSubcategoryId : 0,
	_oWookmarkPlugin : null,
	_oLoading : null,
	_bIsGetReviewProcessing : false,
	_nPage : 1,
	
	$init : function(oOption) {
		jindo.$Fn(this.initialize, this).attach(window, "load");
		jindo.$Fn(this.destroy, this).attach(window, "unload");
		
		this._nCategoryId = oOption.categoryId;
		this._nSubcategoryId = oOption.subcategoryId;
	},
	
	initialize : function() {
		this._oElement = {};
		this._wElement = {};
		this._oTemplate = {};
		this._oEvent = {};
		
		this.setElement();
		this.setEvent();
		
		this._setWookmarkPlugin();
		this._oLoading = new jindo.m.Loading();
	},
	
	setElement : function() {
		this._oElement['sideMenuOfCategory'] = jindo.$$("._sideMenuOfCategory");
		this._oElement['sideMenuOfSubcategory'] = jindo.$$("._sideMenuOfSubcategory");
		this._oElement['sideMenuOfBrand'] = jindo.$$("._sideMenuOfBrand");	
		this._oElement['sideMenuOfSubcategoryArea'] = jindo.$$("._sideMenuOfSubcategoryArea");
		this._oElement['tiles'] = jindo.$("tiles");
		this._oElement["nameText"] = jindo.$$("span._nameText");
		
		this._oTemplate['reviewTemplate'] = jindo.$Template("review_template");
		this._oTemplate['emptyReviewTemplate'] = jindo.$Template("empty_review_template");
	},
	
	setEvent : function() {
		this._oEvent["scrollWindow"] = jindo.$Fn(this._onScrollWindow, this).attach(window, "scroll");
		
		if (this._oElement['sideMenuOfCategory']) {
			this._oEvent["clickSideMenuOfCategory"] = jindo.$Fn(this._onClickSideMenuOfCategory, this).attach(this._oElement['sideMenuOfCategory'], "click");
		}
		
		if (this._oElement['sideMenuOfSubcategory']) {
			this._oEvent["clickSideMenuOfSubcategory"] = jindo.$Fn(this._onClickSideMenuOfSubcategory, this).attach(this._oElement['sideMenuOfSubcategory'], "click");
		}
		
		if (this._oElement['sideMenuOfBrand']) {
			this._oEvent["clickSideMenuOfBrand"] = jindo.$Fn(this._onClickSideMenuOfBrand, this).attach(this._oElement['sideMenuOfBrand'], "click");
		}
		
		if (this._oElement['tiles']) {
			this._oEvent["clickTiles"] = jindo.$Fn(this._onClickTiles, this).attach(this._oElement['tiles'], "click");
		}
	},
	
	_onClickTiles : function(oEvent) {
		var el = oEvent.element;
		if (!el) {
			oEvent.stop();
			return;
		}
	
		var wel = jindo.$Element(el);

		var aWelParent = wel.parent(function(v){
            return v.hasClass("_clickable_area");
        });
		
		if (aWelParent && aWelParent.length > 0) {
			var welParent = aWelParent[0].parent();
			
			var aParams = oCommon._extractParams(welParent.$value().className);
			var nReviewNo = parseInt(aParams[0]);
			
			location.href = "/showReview.do?reviewNo=" + nReviewNo;
		}
	},
	
	_showSelectedName : function() {
		if (!this._oElement["nameText"]) {
			return;
		}

		var nLengthOfNameText = this._oElement["nameText"].length;
		for (var i = 0; i < nLengthOfNameText; i++) {
			var el = this._oElement["nameText"][i];
			if (!el) {
				continue;
			}
			
			var elParent = el.parentNode;

			var aParams = oCommon._extractParams(elParent.className);
			if (aParams && aParams.length > 1) {
				if (aParams[0] == this._nCategoryId && aParams[1] == this._nSubcategoryId) {
					jindo.$Element(el).addClass("on");
				} else {
					if (jindo.$Element(el).hasClass("on")) {
						jindo.$Element(el).removeClass("on");	
					}
				}
			}
		}

	},
	
	_onClickSideMenuOfCategory : function(oEvent) {
		var el = oEvent.element;
		if (!el) {
			oEvent.stop();
			return;
		}
		
		if (/(?:span)/i.test(el.tagName)) {
			el = el.parentNode;
		}
		
		var aParams = oCommon._extractParams(el.className);
		this._nCategoryId = aParams[0];
		this._nSubcategoryId = aParams[1];
		
		this._showSelectedName();
		
		var elParent = el.parentNode;	
	
		var elSideMenuOfSubcategoryArea = jindo.$$.getSingle("._sideMenuOfSubcategoryArea", elParent);
		if (!elSideMenuOfSubcategoryArea) {
			oEvent.stop();
			return;
		}
		
		jindo.$ElementList(this._oElement['sideMenuOfSubcategoryArea']).hide();
		jindo.$Element(elSideMenuOfSubcategoryArea).show();
		
		this._clearReviewList();
		this._getReviewList(this._nCategoryId, this._nSubcategoryId, 1);
	},
	
	_onClickSideMenuOfSubcategory : function(oEvent) {
		var el = oEvent.element;
		if (!el) {
			oEvent.stop();
			return;
		}
		
		if (/(?:span)/i.test(el.tagName)) {
			el = el.parentNode;
		}
		
		var aParams = oCommon._extractParams(el.className);
		this._nCategoryId = aParams[0];
		this._nSubcategoryId = aParams[1];
		
		this._showSelectedName();
		
		this._clearReviewList();
		this._getReviewList(this._nCategoryId, this._nSubcategoryId, 1);
	},
	
	_onClickSideMenuOfBrand : function(oEvent) {
		var el = oEvent.element;
		if (!el) {
			oEvent.stop();
			return;
		}
		
		if (/(?:span)/i.test(el.tagName)) {
			el = el.parentNode;
		}
		
		var aParams = oCommon._extractParams(el.className);
		this._nCategoryId = aParams[0];
		this._nSubcategoryId = aParams[1];
		
		this._showSelectedName();
		
		this._clearReviewList();
		this._getReviewList(this._nCategoryId, this._nSubcategoryId, 1);
	},
	
	_setWookmarkPlugin : function() {
		this._oWookmarkPlugin = $('#tiles li').wookmark({
			autoResize: true,
			container: $('#tiles'),
			offset: 12,
			itemWidth: 220
		});
	},
	
	_clearReviewList : function() {
		if (this._oElement['tiles']) {
			jindo.$Element(this._oElement['tiles']).height(0);
			jindo.$Element(this._oElement['tiles']).html("");
		}
		
		this._nPage = 1;
	},
	
	_addReviewList : function(oReviewList) {
		var aImgUrlList = oReviewList.imgUrlList;
		var oReview = oReviewList.review;
		if (!aImgUrlList || !oReview) {
			return;
		}
		
		var sHtml = this._oTemplate["reviewTemplate"].process({
			reviewNo : oReview.reviewNo,
			imgUrl : aImgUrlList[0] || "",
			isRecomm : oReview.isRecomm,
			title : oReview.title,
			skinType : oReview.skinType,
			hit : oReview.hit,
			writeTime : oReview.writeTime,
			grade : oReview.grade,
			like : oReview.like,
			dislike : oReview.disLike
		});
		
		if (this._oElement['tiles']) {
			jindo.$Element(this._oElement['tiles']).appendHTML(sHtml);
		}
	},
	
	_getReviewList : function(nD1Id, nD2Id, nPage) {
		if (this._bIsGetReviewProcessing) {
			return;
		}
		
		this._bIsGetReviewProcessing = true;
		
		this._oLoading.show();
		
		var oParams = {
			d1Id : nD1Id,
			d2Id : nD2Id,
			page : nPage,
			isResponseJson : true
		};
		
		var oOption = {
			type : "xhr",
			method : "post",
			timeout : 5,
			onload : jindo.$Fn(function(oResponse) {
				var oJson = oResponse.json();
				var bIsSuccess = oJson.isSuccess;
				if (!bIsSuccess) {
					alert("목록을 더 불러오는데 실패하였습니다.");
					this._oLoading.hide();
					this._bIsGetReviewProcessing = false;
					return;
				}
				
				var sProcessedReviewList = oJson.processedReviewList;
				if (!sProcessedReviewList) {	
					this._oLoading.hide();
					this._bIsGetReviewProcessing = false;
					return;
				}
				
				var aProcessedReviewList = JSON.parse(sProcessedReviewList);
				if (!aProcessedReviewList) {
					if (this._nPage == 1) {
						var sHtml = this._oTemplate['emptyReviewTemplate'].process();
						if (this._oElement['tiles']) {
							jindo.$Element(this._oElement['tiles']).html(sHtml);
						}
						
					}
					
					this._oLoading.hide();
					this._bIsGetReviewProcessing = false;
					return;
				}
				
				var nLengthOfProcessedReviewList = aProcessedReviewList.length;
				if (nLengthOfProcessedReviewList.length < 1) {
					this._oLoading.hide();
					this._bIsGetReviewProcessing = false;
					return;
				}
				
				for (var i = 0; i < nLengthOfProcessedReviewList; i++) {
					var oProcessedReviewList = aProcessedReviewList[i];
					this._addReviewList(oProcessedReviewList);
				}
				
				this._setWookmarkPlugin();
				
				this._oLoading.hide();
				var that = this;
				setTimeout(function() {
					that._bIsGetReviewProcessing = false;
				}, 300);
			}, this).bind(),
			ontimeout : jindo.$Fn(function(oResponse) {
				alert("리뷰목록을 가져오는데 실패하였습니다. 잠시후 다시 시도해 주시기 바랍니다.");
				this._oLoading.hide();
				this._bIsGetReviewProcessing = false;
			}, this).bind(),
			onerror : jindo.$Fn(function(oResponse) {
				alert("리뷰목록을 가져오는데 실패하였습니다. 잠시후 다시 시도해 주시기 바랍니다.");
				this._oLoading.hide();
				this._bIsGetReviewProcessing = false;
			}, this).bind()
		};
		
		var oAjax = new jindo.$Ajax("/reviewList.do", oOption);
		oAjax.request(oParams);
	},
	
	_onScrollWindow : function(oEvent) {
		var bNeedUpdate = this._needUpdate();
		
		if (bNeedUpdate && this._nCategoryId != 0) {
			if (this._bIsGetReviewProcessing) {
				return;
			}
			
			this._nPage++;
			this._getReviewList(this._nCategoryId, this._nSubcategoryId, this._nPage);
		}
	},
	
	_needUpdate : function() {
		var nHeightOfDocument = jindo.$Document().scrollSize().height;
		var nCurrentHeight = jindo.$Document().scrollPosition().top + jindo.$Document().clientSize().height;
		var nMargin = 1;
		
		var nBoundaryOfUpdate = nHeightOfDocument - nMargin;
		
		if (this._oElement["tiles"] && jindo.$Element(this._oElement["tiles"]).height() <= 0) {
			return false;
		}
		
		return nCurrentHeight > nBoundaryOfUpdate;
	},
	
	destroy : function() {
		this._oElement = this._wElement = this._oEvent = null;
	}
});
	
var oReviewList = new ReviewList({
	<c:if test="${not empty d1Id && d1Id >= 0}">
		categoryId : ${d1Id}
		<c:if test="${not empty d2Id && d2Id >= 0}">
		,	
		</c:if>
	</c:if>
	<c:if test="${not empty d2Id && d2Id >= 0}">
		subcategoryId : ${d2Id}
	</c:if>
});
</script>