package com.crew91.beautiwiki.vo;

import org.springframework.stereotype.Component;

@Component
public class RecommAgeVO {
	
	private int recommAgeId;
	private int reviewNo;
	private int age;

	public RecommAgeVO() {
		super();
	}

	public RecommAgeVO(int reviewNo, int age) {
		super();
		this.reviewNo = reviewNo;
		this.age = age;
	}
	
	public RecommAgeVO(int recommAgeId, int reviewNo, int age) {
		super();
		this.recommAgeId = recommAgeId;
		this.reviewNo = reviewNo;
		this.age = age;
	}

	public int getRecommAgeId() {
		return recommAgeId;
	}

	public void setRecommAgeId(int recommAgeId) {
		this.recommAgeId = recommAgeId;
	}

	public int getReviewNo() {
		return reviewNo;
	}

	public void setReviewNo(int reviewNo) {
		this.reviewNo = reviewNo;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	@Override
	public String toString() {
		return "RecommAgeVO [recommAgeId=" + recommAgeId + ", reviewNo="
				+ reviewNo + ", age=" + age + "]";
	}

	
}
