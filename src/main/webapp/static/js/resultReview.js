function _bindEventLisener() {
	$(".zzim").click(function (oEvent) {
		_addMyReview(oEvent); //리뷰담는 기능
	}); 
	$(".up_img").click(function (oEvent){
		_recommReview(oEvent);
	});
	$(".down_img").click(function (oEvent){
		_notRecommReview(oEvent);
	});
};

/*
 * 리뷰보관함에 리뷰를 넣는 기능
 */
function _addMyReview(oEvent) {
	
	var nReviewNo = $("#reviewNumber").val();
	$.ajax({
	         url : "addMyReview.do",
	         data : {
	            reviewNo : parseInt(nReviewNo),
	         },
	         type : "get",
	         dataType : "json",
	         success : function(responseData){
	            if(responseData.isSuccess) {
	            	alert(responseData.successMessage);
	            } else {
	            	alert(responseData.errorMessage);
	            }
	         }//end of success
	      },false)   
	      .fail(function(){
	         alert("리뷰저장에 실패했습니다.");   
	      })
	      .error(function(){
	         alert("리뷰저장에 실패했습니다.");
	      });
}

//추천수 올리는 함수
function _recommReview(oEvent) {
	var nReviewNo = $("#reviewNumber").val();
	console.log($("#reviewNumber"));
	console.log(nReviewNo);
	console.log(typeof nReviewNo);
	$.ajax({
	         url : "recommReview.do",
	         data : {
	            reviewNo : parseInt(nReviewNo),
	         },
	         type : "get",
	         dataType : "json",
	         success : function(responseData){
	            if(responseData.isSuccess) {
	            	alert(responseData.successMessage);
	            } else {
	            	alert(responseData.errorMessage);
	            }
	         }//end of success
	      },false)   
	      .fail(function(){
	         alert("리뷰추천에 실패했습니다.");   
	      })
	      .error(function(){
	         alert("리뷰추천에 실패했습니다.");
	      });
}

//비추천수 올리는 함수
function _notRecommReview(oEvent) {
	var nReviewNo = $("#reviewNumber").val();
	
	$.ajax({
	         url : "notRecommReview.do",
	         data : {
	            reviewNo : parseInt(nReviewNo),
	         },
	         type : "get",
	         dataType : "json",
	         success : function(responseData){
	            if(responseData.isSuccess) {
	            	alert(responseData.successMessage);
	            } else {
	            	alert(responseData.errorMessage);
	            }
	         }//end of success
	      },false)   
	      .fail(function(){
	         alert("리뷰비추천에 실패했습니다.");   
	      })
	      .error(function(){
	         alert("리뷰비추천에 실패했습니다.");
	      });
}