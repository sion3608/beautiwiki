<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
<c:set var="currentPageCss" value="comment.css" scope="request"/>
<title>Insert title here</title>
</head>
<body>
<script language = "javaScript">
function fix(){
	f.action = "commentFix.do";
	f.submit();
}

function deleteC(){
	f.action = "deleteComment.do";
	f.submit();
}

</script>
<table>
	<tr>
		<td>리뷰번호 : ${review.reviewNo}</td>
		<td>title : ${review.title}</td>
	</tr>
	<tr>
		<tr>임시 리뷰</tr>
</table>

<section id = "container">
	<div class = "wrap" text-align = "center" style = "width:50%; margin:0 auto;">
		<p>
			<h5><font color = "#5D5D5D">댓글 달기</font></h5>
			
			<form action = "insertComment.do" method = "post">
			<input type = "hidden" name = "reviewNo" value = "${review.reviewNo}">
			<input type = "hidden" name = "userNo" value = "${userNo}">
			<table>
				<tr>
					<td>
						<h6><font color = "grey">${userId}</font></h6>&nbsp;&nbsp;&nbsp;&nbsp;
					</td>
					<td width = "500" align = center>
						<textarea rows = "2" cols="70" name = comment value = "" placeholder = "댓글을 입력해주세요."></textarea>
					</td>
					<td>
						<input type = submit value = "댓글달기">
					</td>
			</table>
		</form>
		</p>
	</div>
	<div class = "wrap" text-align = "center" style = "width:50%; margin:0 auto;">
	<table border = 0>
		<c:forEach items="${commentlist}" var="comment" varStatus = "status">
			<div class = "line" text-align = "center" style = "width:50%; margin:0 auto;">
				<p>
					<tr>
						<td>
							<font size = "2" color = "grey"><b>${comment.userId}</b></font><br>
						</td>
					</tr>
					<tr>
						<td>
							<font color = "grey">${comment.contents}</font><br>
						</td>
					</tr>
					<tr>
						<td width = "600">
							<font size = "2" color = "grey"><b><font color = "#6799FF">${comment.date}</b></font>
						</td>
					<c:choose>
						<c:when test="${comment.userId eq userId}">
							<td>
								<form name = "f" method = post action = "commentFix.do">
									<input type = hidden name = "reviewNo" value = "${review.reviewNo}">
									<input type = hidden name = userNo value = "${userNo}">
									<input type = hidden name = commentNo value = "${comment.comment_no}">
									<input type = hidden name = "contents" value = "${comment.contents}">
									<input type = submit value = "수정">
								</form>
							</td>
							<td>
								<form name = "f2" method = post action = "commentFix.do">
									<input type = hidden name = "reviewNo" value = "${review.reviewNo}">
									<input type = hidden name = commentNo value = "${comment.comment_no}">
									<input type = submit value = "삭제">
								</form>
							</td>
						</c:when>
					</c:choose>
					</tr>
					<tr>
						<td colspan = 3><hr size = "1" color = "grey"></td>
					</tr>
				</p>
		</c:forEach>
	</table>
	</div>
</section>


</body>
</html>