package com.crew91.beautiwiki.vo;

import org.springframework.stereotype.Component;

@Component
public class CategoryVO {
   private int categoryId;
   private String descriptor;
   private String name;
   
   public CategoryVO() {
      super();
   }

   public CategoryVO(int categoryId, String descriptor, String name) {
      super();
      this.categoryId = categoryId;
      this.descriptor = descriptor;
      this.name = name;
   }

   
   public int getCategoryId() {
      return categoryId;
   }

   public void setCategoryId(int categoryId) {
      this.categoryId = categoryId;
   }

   public String getDescriptor() {
      return descriptor;
   }

   public void setDescriptor(String descriptor) {
      this.descriptor = descriptor;
   }

   public String getName() {
      return name;
   }

   public void setName(String name) {
      this.name = name;
   }

   @Override
   public String toString() {
      return "CateogryVO [categoryId=" + categoryId + ", descriptor=" + descriptor
            + ", name=" + name + "]";
   }
   
}