var Common = jindo.$Class({
	_oElement : null,
	_wElement : null,
	_oEvent : null,
	_oTransition : null,
	
	$init : function() {
		jindo.$Fn(this.initialize, this).attach(window, "load");
		jindo.$Fn(this.destroy, this).attach(window, "unload");
	},
	
	initialize : function() {
		this._oElement = {};
		this._wElement = {};
		this._oEvent = {};
		
		this.setElement();
		this.setEvent();
		
		this._oTransition = new jindo.Transition();
	},
	
	setElement : function() {
		this._oElement["categories"] = jindo.$$("div._category");
		this._oElement["subcategories"] = jindo.$$("div._subcategory");
		
		this._wElement["navigationBar"] = jindo.$Element(jindo.$$.getSingle("div._navigationBar"));
		this._wElement["subNavigationBar"] = jindo.$Element(jindo.$$.getSingle("div._subNavigationBar"));
		this._wElement["content"] = jindo.$Element(jindo.$$.getSingle("div.content"));
//		this._wElement["toggleZzimListBtn"] = jindo.$Element(jindo.$$.getSingle("div.toggle_zzim_list_btn"));
//		this._wElement["zzimList"] = jindo.$Element(jindo.$$.getSingle("div.zzim_list"));
	},
	
	setEvent : function() {
		this._oEvent["scrollWindow"] = jindo.$Fn(this._onScrollWindow, this).attach(window, "scroll");
		if (this._wElement["navigationBar"]) {
			this._oEvent["mouseOverNavigationBar"] = jindo.$Fn(this._onMouseOverNavigationBar, this).attach(this._wElement["navigationBar"].$value(), "mouseover");
		}
		if (this._wElement["subNavigationBar"]) {
			this._oEvent["mouseOutSubNavigationBar"] = jindo.$Fn(this._onMouseOutSubNavigationBar, this).attach(this._wElement["subNavigationBar"].$value(), "mouseout");
		}
		if (this._oElement["categories"]) {
			this._oEvent["mouseOverCategories"] = jindo.$Fn(this._onMouseOverCategories, this).attach(this._oElement["categories"], "mouseover");
			this._oEvent["mouseOutCategories"] = jindo.$Fn(this._onMouseOutCategories, this).attach(this._oElement["categories"], "mouseout");
			this._oEvent["clickCategories"] = jindo.$Fn(this._onClickCategories, this).attach(this._oElement["categories"], "click");
		}
		if (this._oElement["subcategories"]) {
			this._oEvent["clickSubcategories"] = jindo.$Fn(this._onClickSubcategories, this).attach(this._oElement["subcategories"], "click");
		}
//		if (this._wElement["toggleZzimListBtn"]) {
//			this._oEvent["clickToggleZzimListBtn"] = jindo.$Fn(this._onClickToggleZzimListBtn, this).attach(this._wElement["toggleZzimListBtn"].$value(), "click");
//		}
	},
	
	_showSubNavigationBar : function() {
		if (this._wElement["subNavigationBar"] && !this._wElement["subNavigationBar"].visible()) {
			this._wElement["subNavigationBar"].show();
		}
	},
	
	_hideSubNavigationBar : function() {
		if (this._wElement["subNavigationBar"] && this._wElement["subNavigationBar"].visible()) {
			this._wElement["subNavigationBar"].hide();
		}
	},
	
//	_onClickToggleZzimListBtn : function(oEvent) {
//		this._oTransition.start(1000,
//			this._wElement["zzimList"].$value(), {
//		        '@left' : '50px'
//			}   	
//		);
//	},
	
	_onClickCategories : function(oEvent) {
		var el = oEvent.element;
		if (!el) {
			oEvent.stop();
			return;
		}
		
		var params = this._extractParams(el.className);
		if (!params || params.length < 2) {
			oEvent.stop();
			return;
		}
		
		var nD1Id = params[0] || 0;
		var nD2Id = params[1] || 0;
		
		if (nD1Id == 0) {
			oEvent.stop();
			return;
		}
		
		location.href = "/reviewList.do?d1Id=" + nD1Id + "&d2Id=" + nD2Id;
		
		oEvent.stop();
	},
	
	_onClickSubcategories : function(oEvent) {
		var el = oEvent.element;
		if (!el) {
			oEvent.stop();
			return;
		}
		
		var params = this._extractParams(el.className);
		if (!params || params.length < 2) {
			oEvent.stop();
			return;
		}
		
		var nD1Id = params[0] || 0;
		var nD2Id = params[1] || 0;
		
		location.href = "/reviewList.do?d1Id=" + nD1Id + "&d2Id=" + nD2Id;
		
		oEvent.stop();
	},
	
	_onMouseOverCategories : function(oEvent) {
		var elCurruntElement = oEvent.element;
		if (!elCurruntElement) {
			oEvent.stop();
			return;
		}
		
		if (/(?:span)/i.test(elCurruntElement.tagName)) {
			elCurruntElement = elCurruntElement.parentNode;
		}
		
		if (/(?:div)/i.test(elCurruntElement.tagName) && jindo.$Element(elCurruntElement).hasClass("_category")) {
			var welSubCategoryArea = jindo.$Element(jindo.$$.getSingle("div._subcategoryArea", elCurruntElement));
			if (!welSubCategoryArea) {
				oEvent.stop();
				return;
			}
			
			welSubCategoryArea.show();
		}
		
		oEvent.stop();
	},
	
	_onMouseOutCategories : function(oEvent) {
		var welCurrentElement = jindo.$Element(oEvent.element);
		if (!welCurrentElement) {
			oEvent.stop();
			return;
		}
		
		var welRelatedElement = jindo.$Element(oEvent.relatedElement);
		if (!welRelatedElement) {
			oEvent.stop();
			return;
		}
		
		if (welRelatedElement.hasClass("_bottomLine")) {
			oEvent.stop();
			return;
		}
		
		var aWelParent = welCurrentElement.parent(function(v){
            return v.hasClass("_category");
        });

		var welCategory = welCurrentElement;
		if (aWelParent.length > 0) {
			welCategory = aWelParent[0];
		}
		
		var bIsChildOfCategory = welRelatedElement.isChildOf(welCategory);
		if (!bIsChildOfCategory) {
			var welSubCategoryArea = jindo.$Element(jindo.$$.getSingle("div._subcategoryArea", welCategory.$value()));
			if (!welSubCategoryArea || !welSubCategoryArea.visible()) {
				oEvent.stop();
				return;
			}
			
			welSubCategoryArea.hide();
		}
		
		oEvent.stop();
	},
	
	_onScrollWindow : function(oEvent) {
		if (this._isTop()) {
			this._showSubNavigationBar();
		} else {
			this._hideSubNavigationBar();
		}
		
		oEvent.stop();
	},
	
	_onMouseOverNavigationBar : function(oEvent) {
		if (this._isTop()) {
			oEvent.stop();
			return;
		}
		
		this._showSubNavigationBar();
		
		oEvent.stop();
	},
	
	_onMouseOutSubNavigationBar : function(oEvent) {
		if (this._isTop()) {
			oEvent.stop();
			return;
		}

		var elRelatedElement = oEvent.relatedElement;
		if (!elRelatedElement) {
			oEvent.stop();
			return;
		}
		
		if (/(?:body)/i.test(elRelatedElement.tagName)) {
			this._hideSubNavigationBar();
		}

		var bIsChildOfContent = jindo.$Element(elRelatedElement).isChildOf(this._wElement["content"]);
		if (bIsChildOfContent) {
			this._hideSubNavigationBar();
		}
		 
		oEvent.stop();
	},
	
	_isTop : function() {
		var nTop = jindo.$Document().scrollPosition().top;
		return nTop == 0;
	},
	
	_extractParams : function(sClassName) {
		return sClassName.replace(/.*_params\('([^\)]+)'\).*/g, '$1').split(',');
	},
	
	destroy : function() {
		this._oElement = this._wElement = this._oEvent = null;
	}
});