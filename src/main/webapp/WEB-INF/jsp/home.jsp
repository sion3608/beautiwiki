<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<c:set var="currentPageCss" value="reviewList.css,home.css" scope="request" />
<jsp:include page="/WEB-INF/jsp/common/header.jsp" />

<div class="section group" style="margin-top:40px;">
	<div>
		<div class="col span_1_of_1">
			<div style="width:0;height:0;margin-top:23px;border-color:#8EB021 #8EB021 transparent transparent;border-style:solid;border-width:5px;float:left"></div>
			<span style="color:#CE483D;margin-top:20px;display:block;font-weight:bold;float:left;margin-left:5px;">추천 화장품</span>
			<img src="/static/img/medal.png" height="35" width="35" style="margin-top:10px;">
			
			<div class="tile">	
				<c:if test="${not empty matchingList}">
					<c:forEach var="matchedCosmetic" items="${matchingList}" varStatus="sequence">
						<div style="cursor:pointer;"class="mached_cosmetic_area clickCosNo _params('${matchedCosmetic.cosmeticsNo}')">
							<div class="matched_cosmetic_img_area ">
								<img src="${matchedCosmetic.imgUrl}" width="100%"/>
							</div>
							<div class="matched_cosmetic_brand_area">
								<span style="font-weight:bold;font-size:13px;">${matchedCosmetic.brandName }</span>
							</div>
							<div class="matched_cosmetic_title_area">
							
							<span style="font-weight:bold;font-size:15px;color:#CE483D">${matchedCosmetic.name }</span>
							</div>
							
							<div class="matched_cosmetic_price_area">
								<span style="color:#19BDC4;font-weight:bold;font-size:13px;">${matchedCosmetic.price }원 (${matchedCosmetic.volume } ml)</span>
							</div>
							<!-- <div style="clear:both;height:70px;width:70px;position:absolute;top:-10px;left:-10px">
								<img src="/static/img/medal.png" height="100%" width="100%">
						    </div>
						     -->
						</div>					
					</c:forEach>
				</c:if>
			</div>
		</div>
	</div>
	<div>
		<div class="col span_1_of_1">
			<div style="width:0;height:0;margin-top:23px;border-color:#8EB021 #8EB021 transparent transparent;border-style:solid;border-width:5px;float:left"></div>
			<span style="color:#CE483D;margin-top:20px;display:block;font-weight:bold;float:left;margin-left:5px;">Hot 리뷰</span>
			<img src="/static/img/fire.png" height="30" width="30" style="margin-top:10px;">
		
			<div class="tile" style="position:relative">
				<c:if test="${not empty highHitReviewList}">
					<div id="rolling" class="rolling">
					    <ul>
					        <c:forEach var="highHitReview" items="${highHitReviewList}" varStatus="sequence">
					        	<li class="clickReviewNo _params('${highHitReview.review.reviewNo}')" style="position:relative;cursor:pointer">
						        	<table style="width:100%">
						        		<tr>
						        			<td width="70%" style="padding-left:100px;">
						        				<!-- 제목 -->
						        				<div>
						        					<span style="font-weight:bold;font-size:20px;">${highHitReview.review.title}</span>
						        				</div>
						        				
						        				<!-- 조회수 -->
						        				<!-- 작성 날짜 -->
						        				<div style="margin-top:15px;border-bottom:1px solid #d3d3d3;padding-bottom:15px;">
						        					조회수 <span class="point">${highHitReview.review.hit}</span> <span class="division">|</span> <fmt:formatDate value="${highHitReview.review.writeTime}" pattern="yyyy.MM.dd"></fmt:formatDate>
						        				</div>
							        			<div style="margin-top:20px;">
							        				<!-- 등급 -->
							        				<div style="display:inline-block;margin-right:20px;text-align:center;">
							        					<div class="grade_circle"></div>
							        					<span>별점</span>
							        				</div>
							        				
							        				<!-- 좋아요 -->
							        				<div style="display:inline-block;margin-right:20px;text-align:center;">
							        					<div class="like_circle"></div>
							        					<span>좋아요</span>
							        				</div>
							        				
							        				<!-- 싫어요 -->
							        				<div style="display:inline-block;text-align:center;">
						        						<div class="disLike_circle"></div>
						        						<span>싫어요</span>
						        					</div>
						        				</div>
						        			</td>
						        			<td>
						        				<c:if test="${not empty highHitReview.imgUrlList}">
					                              <c:forEach var="imgUrl" items="${highHitReview.imgUrlList}" varStatus="sequence">
					                                 <c:if test="${sequence.count eq 1}">
					                                 	<img src="${imgUrl}" height="385px">	
					                                 </c:if>
					                              </c:forEach>
					                           </c:if>
						        			</td>
						        		</tr>
						        	</table>
						        	<div style="clear:both;height:0;width:0;border-color:#CE483D transparent transparent #CE483D;border-style:solid;border-width:50px;position:absolute;top:0;left:0">
						        	</div>
						        	<!-- <div style="clear:both;height:70px;width:70px;position:absolute;top:-10px;left:-5px;">
										<img src="/static/img/fire.png" height="100%" width="100%">
								    </div> -->
					        	</li>
					        </c:forEach>
					    </ul>
					</div>
				</c:if>
				<div onClick="oCircularRolling.moveBy(-1); return false;" style="position:absolute;top:180px;height:80px;width:50px;left:0"><img src="/static/img/left.png" height="100%" width="100%"></div>
				<div onClick="oCircularRolling.moveBy(1); return false;" style="position:absolute;top:180px;height:80px;width:50px;right:0"><img src="/static/img/right.png" height="100%" width="100%"></div>
				<%-- 
				<c:if test="${not empty highHitReviewList}">
					<c:forEach var="highHitReview" items="${highHitReviewList}" varStatus="sequence">
						<span>${highHitReview.title}</span>					
					</c:forEach>
				</c:if>
				--%>
				
				<!-- 
				reviewNo
				userNo
				cosmeticsNo
				title
				contents
				grade
				isRecomm
				skinType
				writeTime
				hit
				like
				disLike
				imgNames
				 -->
			</div>
		</div>
	</div>
	<div>
		<div class="col span_1_of_1">
			<div style="width:0;height:0;margin-top:23px;border-color:#8EB021 #8EB021 transparent transparent;border-style:solid;border-width:5px;float:left"></div>
			<span style="color:#CE483D;margin-top:20px;display:block;font-weight:bold;float:left;margin-left:5px;">최신 리뷰</span>
		
			<div class="tile">
				<c:if test="${not empty newestReviewList}">
					<ul id="tiles2">
						<c:forEach var="newestReview" items="${newestReviewList}" varStatus="sequence">
			               <li style="float:left;width:226px;cursor:pointer;"<c:if test="${not empty newestReview.review}"> class="clickReviewNo _params('${newestReview.review.reviewNo}')"</c:if>>
			                  <div style="width: 220px;background-color: #ffffff;border: 1px solid;border-color: #e9eaed #dfe0e4 #d0d1d5;border-radius: 2px;-moz-border-radius: 2px;-webkit-border-radius: 2px;cursor: pointer;border-radius: 0.2em;">
			                     <c:if test="${not empty newestReview.review}">
			                        <div class="_clickable_area" style="position:relative">
			                           <c:if test="${not empty newestReview.imgUrlList}">
			                              <c:forEach var="imgUrl" items="${newestReview.imgUrlList}" varStatus="sequence">
			                                 <c:if test="${sequence.count eq 1}">
			                                    <div class="review_img_area">
			                                       <img class="review_img" src="${imgUrl}" width="220" height="300">
			                                    </div>
			                                 </c:if>
			                              </c:forEach>
			                           </c:if>
			                           
			                           <div class="review_content_area">
			                              <div class="review_recomm_area">
			                                 <c:if test="${newestReview.review.isRecomm eq 1}">
			                                    <img class="recommend_img" src="/static/img/smile.png" width="20" height="20">
			                                 </c:if>
			                                 <c:if test="${newestReview.review.isRecomm eq 0}">
			                                    <img class="recommend_img" src="/static/img/sad.png" width="20" height="20">
			                                 </c:if>
			                              </div>
			                              
			                              <div class="review_title_area" style="height:20px;overflow:hidden;">
			                                 ${newestReview.review.title}
			                              </div>
			                              
			                              <div class="review_hit_and_writedate_area">
			                                 조회수 <span class="point">${newestReview.review.hit}</span> <span class="division">|</span> <fmt:formatDate value="${newestReview.review.writeTime}" pattern="yyyy.MM.dd"></fmt:formatDate>
			                              </div>
			                           </div>
			                           
			                           <div class="review_grade_area">
			                              <c:forEach var="grade" begin="1" end="5" step="1">
			                                 <c:set var="tempGrade" value="${6 - grade}"/>
			                                 <c:if test="${newestReview.review.grade <= tempGrade}">
			                                    <img style="float:left" class="grade_img" src="/static/img/star_on.png" width="15" height="15">
			                                 </c:if>
			                                 <c:if test="${newestReview.review.grade > tempGrade}">
			                                    <img style="float:left" class="grade_img" src="/static/img/star_off.png" width="15" height="15">
			                                 </c:if>
			                              </c:forEach>
			                           </div>
			                           
			                           <!-- <div style="clear:both;height:0;width:0;border-color:#CE483D transparent transparent #CE483D;border-style:solid;border-width:50px;position:absolute;top:0;left:0">
						        		</div> -->
			                        </div>
			                              
			                        <div class="review_like_and_dislike">
			                           <table style="width:100%">
			                              <tr>
			                                 <td>
			                                    좋아요 <span class="point">${newestReview.review.like}</span>   
			                                 </td>
			                                 <td>
			                                    싫어요 <span class="point">${newestReview.review.disLike}</span>
			                                 </td>
			                                 <td>
			                                    공유하기
			                                 </td>
			                              </tr>
			                           </table>
			                        </div>
			                     </c:if>
			                  </div><!-- add div -->
			               </li>
			            </c:forEach>
					</ul>
				</c:if>
				<c:if test="${empty newestReviewList}">
			      <div id="noResult">검색결과가 존재하지 않습니다.</div>
			   </c:if>
			</div>
		</div>
	</div>
</div>

<jsp:include page="/WEB-INF/jsp/common/footer.jsp" />
<script type="text/javascript" src="${JS_DIR}/jquery-1.10.1.js"></script>
<script type="text/javascript" src="${JS_DIR}/circle-progress.js"></script>
<script type="text/javascript">
$(".clickReviewNo").click(function (oEvent){
	_clickReviewNo(oEvent);
});

$(".clickCosNo").click(function (oEvent){
	_clickCosNo(oEvent);
});

function _clickReviewNo(oEvent){
   var elCurrentTarget = oEvent.currentTarget;
   if (!elCurrentTarget) {
      oEvent.stopPropagation();
      return;
   }
   
   var params = parseInt(extractParams(elCurrentTarget.className));
   location.href = "showReview.do?reviewNo=" + params;
}

function _clickCosNo(oEvent){
	   var elCurrentTarget = oEvent.currentTarget;
	   if (!elCurrentTarget) {
	      oEvent.stopPropagation();
	      return;
	   }
	   
	   var params = parseInt(extractParams(elCurrentTarget.className));
	   location.href = "showCosmetics.do?cosmeticsNo=" + params;
}
function extractParams(sClassName) {
	   return sClassName.replace(/.*_params\('([^\)]+)'\).*/g, '$1').split(',');
};
</script>
<script type="text/javascript">
var oCircularRolling = new jindo.CircularRolling(jindo.$('rolling'));
$('.grade_circle').circleProgress({
    value: 0.75,
    size: 100,
    fill: {
        gradient: ["red", "orange"]
    }
});

$('.like_circle').circleProgress({
    value: 0.75,
    size: 100,
    fill: {
        gradient: ["yellow", "green"]
    }
});

$('.disLike_circle').circleProgress({
    value: 0.75,
    size: 100,
    fill: {
        gradient: ["blue", "navy"]
    }
});
</script>