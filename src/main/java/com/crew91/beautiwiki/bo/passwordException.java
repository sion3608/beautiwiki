package com.crew91.beautiwiki.bo;

import javax.swing.JOptionPane;

public class passwordException extends Exception{
	public passwordException() {
		super("회원가입시 비밀번호 불일치 예외");
		JOptionPane.showMessageDialog(null, "비밀번호가 일치하지 않습니다.", "비밀번호경고", JOptionPane.WARNING_MESSAGE);
	}
}
