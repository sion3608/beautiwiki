<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="currentPageCss" value="writeReview.css,bootstrap.css" scope="request" />
<c:set var="title" value="createReview" />
<%@include file="common/header.jsp"%>
<script type="text/javascript" src="editor/js/HuskyEZCreator.js" charset="utf-8"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery/1.9.0/jquery.js"></script>
</head>
<body>	
<div class="content-area">
<form id="frm" action="saveReview.do" method="post">
	<div id="wrapReviewHead">
		<div id="reviewHead">리뷰 등록</div><br>
	</div>
	<div style="margin-top: 10px;margin-bottom: 10px;">
		<span id="detailReviewHead">사용하신 화장품의 솔직한 후기를 작성해주세요!</span>
		<span id="searchedCos">리뷰대상 상품</span>
	</div>
	<table width="100%" margin="auto">	
			<tr>
				<td>
					<div class="btn-group dropdown branddown">
					  <button type="button" id="brandBtn" class="btn btn-default dropdown-toggle dropdown" data-toggle="dropdown">
					    브랜드를 선택하세요
					    <span class="caret"></span>
					    </button>
					  <ul class="dropdown-menu brand-menu branddown" role="menu">
					   		<c:forEach items="${allBrandList}" var="brand" >
								<li class="_dropdownBrand _params('${brand.brandId},${brand.name}')">${brand.name}</li>
							</c:forEach>
					  </ul>
					  <input id="brand" type="hidden" name="brandId" value="default">
					</div><!-- brandSelectBox end -->	
				</td>
				<td rowspan="4"><div id="cosArea" style="background-color:#fff;margin-left: 40px;width:600px;height: 200px;border:1px solid #ccc;overflow:auto"></div></td>
			</tr>
			
			<tr style="position:relative;">	
				<td>
						<div class="btn-group dropdown">	
						  <button type="button" id="catBtn" class="btn btn-default dropdown-toggle dropdown" data-toggle="dropdown">
						    1차 카테고리
						    <span class="caret"></span>
						    </button>
						  <ul class="dropdown-menu cat-menu" role="menu">
						   		<c:forEach items="${allCatList}" var="cat" >
									<li class="_dropdownCat _params('${cat.categoryId},${cat.name}')">${cat.name}</li>
								</c:forEach>
						  </ul>
						  <input id="cat" type="hidden" name="catId" value="default">
						</div><!-- catSelectBox end -->
				</td>
			</tr>
			<tr>	
				<td>		
						<div class="btn-group dropdown">
							  <button type="button" id="subCatBtn" class="btn btn-default dropdown-toggle dropdown" data-toggle="dropdown">
							    2차 카테고리
							    <span class="caret"></span>
							    </button>
							  <ul class="dropdown-menu subCat-menu" role="menu">
							  </ul>
							  <input id="subCat" type="hidden" name="subCatId" value="default">
						</div><!-- subCatSelectBox end -->
				</td>
			</tr>
			<tr>
				<td>		
					<input type="button"class="btn btn-default" value="검색" id="cosFindBtn">
				</td>
			</tr>
		<tr>
			<td colspan="2" style="padding-top: 20px;">
				<div class="form-group">
    				<label for="titleLabel" style="font-size: 20px;">Reveiw Title</label>
    				<input type="email" class="form-control" id="inputTitle" name="title" placeholder="Enter title">
  				</div>
  			</td>
		</tr>
		<tr>
			<td colspan="2">
				<select name="skinType" class="selectBox">
					<option value="default">피부타입</option>
					<option value="건성">건성</option>
					<option value="지성">지성</option>
					<option value="중성">중성</option>
				</select>
				<select name="isRecomm" class="selectBox">
					<option value="0">비추천</option>
					<option value="1">추천</option>
				</select>
				<select name="grade" class="selectBox">
					<option value="1">1점</option>
					<option value="2">2점</option>
					<option value="3">3점</option>
					<option value="4">4점</option>
					<option value="5">5점</option>
				</select>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<ul class='select_subject'>
					<label>추천나이</label>
					<label><input type='checkbox' class='input_check' name='generation' value='10' /> 10대</label>
					<label><input type='checkbox' class='input_check' name='generation' value='20' /> 20대</label>
					<label><input type='checkbox' class='input_check' name='generation' value='30' /> 30대</label>
					<label><input type='checkbox' class='input_check' name='generation' value='40' /> 40대</label>
					<label><input type='checkbox' class='input_check' name='generation' value='50' /> 50대 이상</label>
				</ul>
			</td>	
		</tr>
		<tr>
			<td colspan="2">
				<textarea rows="10" cols="30" id="ir1" name="textAreaContent" style="width:100%; height:412px; "></textarea>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<input type="button" class="resBtn" id="save" value="저장"/>
				<input type="button" class="resBtn" value="취소"/>
			</td>
		</tr>
	</table>
	<input type="hidden" name="imgNames" id="imgNames" value=""/>
</form>
</div>

<%@include file="common/footer.jsp"%>
<script type="text/javascript" src="${JS_DIR}/jquery-1.10.1.js"></script>
<script src="${JS_DIR}/writeReview.js"></script>
<script type="text/javascript" src="${JS_DIR}/bootstrap.js"></script>
<script type="text/javascript">
$(document).ready (function () {
	_bindEventLisener();
});

var oEditors = [];
$(function(){
					nhn.husky.EZCreator.createInIFrame({
						oAppRef: oEditors,
						elPlaceHolder: "ir1",
						//SmartEditor2Skin.html 파일이 존재하는 경로
						sSkinURI: "editor/SmartEditor2Skin.html",	
						htParams : {
							// 툴바 사용 여부 (true:사용/ false:사용하지 않음)
							bUseToolbar : true,				
							// 입력창 크기 조절바 사용 여부 (true:사용/ false:사용하지 않음)
							bUseVerticalResizer : true,		
							// 모드 탭(Editor | HTML | TEXT) 사용 여부 (true:사용/ false:사용하지 않음)
							bUseModeChanger : true,			
							fOnBeforeUnload : function(){
								
							}
						}, 
						fOnAppLoad : function(){
							//기존 저장된 내용의 text 내용을 에디터상에 뿌려주고자 할때 사용
							//oEditors.getById["ir1"].exec("PASTE_HTML", ["기존 DB에 저장된 내용을 에디터에 적용할 문구"]);
						},
						fCreator: "createSEditor2"
					});
});

$("#save").click(function(){
	oEditors.getById["ir1"].exec("UPDATE_CONTENTS_FIELD", []);
	$("#frm").submit();
});

// textArea에 이미지 첨부
function pasteHTML(filepath){
    var sHTML = '<img src="<%=request.getContextPath()%>/editor/upload/'+filepath+'">';
    oEditors.getById["ir1"].exec("PASTE_HTML", [sHTML]);
    
    var sImageNames = jindo.$("imgNames").value;
    sImageNames += (sImageNames === "") ? filepath : "|" + filepath;
    jindo.$("imgNames").value = sImageNames;
}
</script>