<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
<title>Insert title here</title>
</head>
<body>
<%--리뷰 내용 출력후 --%>
<table>
	<tr>
		<td>리뷰번호 : ${review.reviewNo}</td>
		<td>title : ${review.title}</td>
	</tr>
	<tr>
		<tr>임시 리뷰</tr>
</table>

<%--아래에는 댓글을 수정할수 있게  선택한 부분만 버튼이 나오게 했다. --%>

<section id = "container">
	<div class = "wrap" text-align = "center" style = "width:50%; margin:0 auto;">
		<p>
			<h5><font color = "#5D5D5D">댓글 달기</font></h5>
				
				<form action = "insertComment.do" method = "post">
				<input type = "hidden" name = "reviewNo" value = "${review.reviewNo}">
				<input type = "hidden" name = "userNo" value = "${userNo}">
				<table>
					<tr>
						<td>
							<h6><font color = "grey">${userId}</font></h6>&nbsp;&nbsp;&nbsp;&nbsp;
						</td>
						<td width = "500" align = center>
							<textarea rows = "2" cols="70" name = comment value = "" placeholder = "댓글을 입력해주세요."></textarea>
						</td>
						<td>
							<input type = submit value = "댓글달기">
						</td>
				</table>
			</form>
		</p>
	</div>
	<div class = "wrap" text-align = "center" style = "width:50%; margin:0 auto;">
		<p>
			<table border = 0>
				<c:forEach items="${commentlist}" var="comment" varStatus = "status">
					<c:choose>
						<c:when test = "${commentNo eq comment.comment_no}">
							<form name = f method = post action = commentFix2.do>
								<input type = hidden name = "reviewNo" value = "${review.reviewNo}">
								<input type = hidden name = userNo value = "${userNo}">
								<input type = hidden name = commentNo value = "${comment.comment_no}">
									<tr>
					    				<td><font size = "2" color = "grey"><b>${comment.userId}</b></font></td>
									</tr>
									<tr>
										<td>
											<textarea rows = "2" cols="70" name = contents>${comment.contents}</textarea>
											<input type = submit value = "수정">
										</td>
									</tr>
									<tr>
										<td width = "600">
											<font size = "2" color = "#6799FF"><b>${comment.date}</b></font>
										</td>
									</tr>
								</form>
					    </c:when>
						<c:otherwise>
							<tr>
					       		<td><font size = "2" color = "grey"><b>${comment.userId}</b></font></td>
				       		</tr>
				       		<tr>
				       			<td><font color = "grey">${comment.contents}</font></td>
							</tr>
							<tr>
								<td><font size = "2" color = "#6799FF"><b>${comment.date}</b></font></td>
							</tr>
				        </c:otherwise>
					</c:choose>
					<tr>
						<td colspan = 3><hr size = "1" color = "grey"></td>
					</tr>
				</c:forEach>
			</table>
		</p>
	</div>
</section>
</body>
</html>