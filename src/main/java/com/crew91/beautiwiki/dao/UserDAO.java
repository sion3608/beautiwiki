package com.crew91.beautiwiki.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import org.apache.ibatis.session.SqlSession;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.crew91.beautiwiki.vo.UserVO;

@Component
public class UserDAO extends SqlSessionDaoSupport{
	@Autowired
    private SqlSession sqlSession;
	@Autowired
	private UserVO userVO;
	@Autowired
	private DataSource ds;
	public UserDAO() throws Exception {
		Context init = new InitialContext();
		ds = (DataSource)init.lookup("java:comp/env/jdbc/OracleDB");
	}

	public UserVO login(String id) throws SQLException{
		Connection conn = null;
		PreparedStatement pstmt = null;
		
		String sql = "SELECT user_no, user_id, password FROM tb_user where user_id=?";
		UserVO uservo = null;
		
		try {
			conn = ds.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, id);
			ResultSet rs = pstmt.executeQuery();
			
			if(rs.next()) {
				uservo = new UserVO();
				uservo.setUserNo(rs.getInt("user_no"));
				uservo.setUserId(rs.getString("user_id"));
				uservo.setPw(rs.getString("password"));
			}
			
		}catch(Exception e) {
			e.printStackTrace();
			
		}finally {
			if(pstmt != null) {
				pstmt.close();
			}
			if(conn != null) {
				conn.close();
			}
		}
		return uservo;
	}
	
	public UserVO getUserInfo(String userId) throws SQLException{//회원정보를가져오는 
		Connection conn = null;
		PreparedStatement pstmt = null;
		String sql = "SELECT * FROM tb_user where user_id=?";
		UserVO uservo = null;
		
		try {
			conn = ds.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, userId);
			ResultSet rs = pstmt.executeQuery();
			
			if(rs.next()) {
				uservo = new UserVO();
				uservo.setUserNo(rs.getInt("user_no"));
				uservo.setUserId(rs.getString("user_id"));
				uservo.setPw(rs.getString("password"));
				uservo.setAge(rs.getInt("age"));
				uservo.setSex(rs.getInt("sex"));
				uservo.setUserName(rs.getString("user_name"));
				uservo.setSkinType(rs.getString("skin_type"));
			}
			
		}catch(Exception e) {
			e.printStackTrace();
			
		}finally {
			if(pstmt != null) {
				pstmt.close();
			}
			if(conn != null) {
				conn.close();
			}
		}
		return uservo;
	}
	
	public boolean updateUser(UserVO user) throws SQLException{
		Connection conn = null;
		PreparedStatement pstmt = null;
		String sql = "UPDATE tb_user SET password ='" + user.getPw() + "',user_name ='" + user.getUserName() + "',skin_type='" +user.getSkinType() + "',age ='" + user.getAge() + "'where user_no =" + user.getUserNo();

		try {
			conn = ds.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.executeUpdate(sql);
						
		}catch(Exception e) {
			e.printStackTrace();
			return false;
			
		}finally {
			if(pstmt != null) {
				pstmt.close();
			}
			if(conn != null) {
				conn.close();
			}
		}
		return true;
	}
	
	public boolean deleteUser(int id) throws SQLException{
		Connection conn = null;
		PreparedStatement pstmt = null;
		String sql = "delete FROM tb_user where user_no=" + id;
				
		try {
			conn = ds.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.executeUpdate(sql);

			
		}catch(Exception e) {
			e.printStackTrace();
			
		}finally {
			if(pstmt != null) {
				pstmt.close();
			}
			if(conn != null) {
				conn.close();
			}
		}
		return true;
	}
	
	public int insert(UserVO userInfo) throws SQLException {
		int ox;
		ox = getSqlSession().insert("tb_user.insertUserinfo", userInfo); 
		if (ox > 0) {
			return userInfo.getUserNo();
		}
		else {
			return -1;
		}
	}
	public UserVO selectUserInfo(String userId) throws SQLException {
		return getSqlSession().selectOne("tb_user.selectUserInfo", userId);
	}
}
