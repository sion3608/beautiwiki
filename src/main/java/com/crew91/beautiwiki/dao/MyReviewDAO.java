package com.crew91.beautiwiki.dao;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.crew91.beautiwiki.vo.MyReviewVO;
import com.crew91.beautiwiki.vo.RecommAgeVO;

@Component
public class MyReviewDAO extends SqlSessionDaoSupport{

	@Autowired
	private SqlSession sqlSession;
	
	/*
	 * params : myReviewVO;
	 * 목적 : 리뷰번호와 로그인한 유저아이디로 찜한 리뷰를 저장한다.
	 */
	public int insertMyreviewByUserNoAndReviewNo(MyReviewVO myReviewVO) throws SQLException{
		return (Integer)getSqlSession().insert("tb_myReview.insertNewMyReviewByUserNOAndReviewNO", myReviewVO);
	}

	public int selectMyReviewByReviewNoAndUserNO(int reviewNo, int userNo)throws SQLException {
		int result ;
		Map<String, Integer> hMap = new HashMap<String, Integer>();
		hMap.put("userNum", userNo);
		hMap.put("reviewNum", reviewNo);
		
		if(getSqlSession().selectOne("tb_myReview.selectMyReviewByReviewNoAndUserNO", hMap) == null) {
			result = 0;
		} else {
			result = -1;
		}
		return result;
	}

	//user가 보관하고 있는 보관하고 있는 리스트를 search해주는 함수
	public List<Integer> selectMyReviewByUserNo(int userNo) {
		return getSqlSession().selectList("tb_myReview.selectMyReviewByUserNo", userNo);
	}

}
