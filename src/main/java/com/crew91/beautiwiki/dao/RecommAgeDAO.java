package com.crew91.beautiwiki.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import org.apache.ibatis.session.SqlSession;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.crew91.beautiwiki.vo.RecommAgeVO;

@Component
public class RecommAgeDAO extends SqlSessionDaoSupport{
	@Autowired
	private SqlSession sqlSession;

	@Autowired
	private DataSource ds;
	
	public RecommAgeDAO() throws Exception {
		Context init = new InitialContext();
		ds = (DataSource)init.lookup("java:comp/env/jdbc/OracleDB");
	}

	public int insertRecommAgeByReviewNO(List<RecommAgeVO> recommAgeList) {
		
		int result = 0;
		for(int i = 0; i < recommAgeList.size(); i++) {
			result += (Integer)getSqlSession().insert("tb_recomm_age.insertRecommAgeByReviewNO", recommAgeList.get(i));
		}
		return result;
	}

	public boolean deleteUserAllRecomm_age(int user_no) throws SQLException{ //회원이 쓴 모든 나이추천을 삭제한다.
		Connection conn = null;
		PreparedStatement pstmt = null;
		
		String sql = "delete FROM tb_recomm_age where review_no in (select review_no from tb_review where user_no = " + user_no + ")";
		try {
			conn = ds.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.executeUpdate(sql);
		}catch(Exception e) {
			e.printStackTrace();
			
		}finally {
			if(pstmt != null) {
				pstmt.close();
			}
			if(conn != null) {
				conn.close();
			}
		}
		return true;
	}
	
	
}
