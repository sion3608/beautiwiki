package com.crew91.beautiwiki.dao;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.crew91.beautiwiki.vo.BrandVO;
import com.crew91.beautiwiki.vo.CategoryVO;
import com.crew91.beautiwiki.vo.CosmeticVO;
import com.crew91.beautiwiki.vo.ReviewVO;
import com.crew91.beautiwiki.vo.SubCategoryVO;

@Component
public class SearchDAO extends SqlSessionDaoSupport{

	@Autowired
    private SqlSession sqlSession;
	
	//1차카테고리정보 모두 셀렉
	public  List<CategoryVO> selectAllCategory() throws SQLException {
		return getSqlSession().selectList("tb_category.selectAll");
	}
	
	//브랜드정보 모두 셀렉
	public  List<BrandVO> selectAllBrand() throws SQLException {
		return getSqlSession().selectList("tb_brand.selectAllBrand");
	}

	// 1차 카테고리 카운트 가져오기
	public int getCategoryCount() throws SQLException {
		return getSqlSession().selectOne("tb_category.count");
	}
	
	/*
	 * 파라미터 타입 : int(브랜드 아이디), int (1차카테고리아이디)
	 * 목적 : 브랜드와 1차카테고리에 속해있는 2차카테고리를 가져온다.
	 */
	public List<SubCategoryVO> selectSubcatByBrandCat(int brandId, int catId) throws SQLException{
		HashMap<String, Integer> hMap = null;
		hMap = new HashMap<String, Integer>();
		hMap.put("brandId", brandId);
		hMap.put("catId", catId);
		return getSqlSession().selectList("tb_subcategory.selectSubcatByBrandCat", hMap);
	}
	
	public List<CosmeticVO> selectCosByBrandSubcat(int brandId, int subcatId) throws SQLException{
		HashMap<String, Integer> hMap = null;
		hMap = new HashMap<String, Integer>();
		hMap.put("brandId", brandId);
		hMap.put("subcatId", subcatId);
		return getSqlSession().selectList("tb_cosmetics.selectCosByBrandSubcat", hMap);
	}
	
	public CosmeticVO selectCosByCosId(int cosmeticsNo) throws SQLException {
		return getSqlSession().selectOne("tb_cosmetics.selectCosByCosId", cosmeticsNo);
	}
	
	// 1차 카테고리 아이디로 2차 카테고리 목록 가져오기
	public List<SubCategoryVO> getSubCategoryByCatId(int catId) throws SQLException {
		return getSqlSession().selectList("tb_subcategory.selectSubCategoryByCatId", catId);
	}

	//키워드로 리뷰 search해주는 함수
	public List<ReviewVO> searchKeywordForReview(String searchKeyword) throws SQLException {
		return getSqlSession().selectList("tb_review.searchKeywordForReview", searchKeyword);
	}
}
