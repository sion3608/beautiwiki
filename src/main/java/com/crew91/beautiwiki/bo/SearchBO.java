package com.crew91.beautiwiki.bo;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.impl.Log4JLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.crew91.beautiwiki.dao.MyReviewDAO;
import com.crew91.beautiwiki.dao.SearchDAO;
import com.crew91.beautiwiki.vo.BrandVO;
import com.crew91.beautiwiki.vo.CategoryVO;
import com.crew91.beautiwiki.vo.CosmeticVO;
import com.crew91.beautiwiki.vo.ReviewVO;
import com.crew91.beautiwiki.vo.SubCategoryVO;

@Component
public class SearchBO {
	public static final Log4JLogger LOG = new Log4JLogger("SearchBO");
	
	@Autowired
	SearchDAO searchDAO;
	@Autowired
	ReviewBO reviewBO;
	@Autowired
	MyReviewDAO myReviewDAO;
	
	
	/**
	 * 1차 카테고리 아이디로 2차 카테고리 목록 조회
	 * 
	 * @param catId
	 * @return
	 */
	public List<SubCategoryVO> getSubCategoryByCatId(int catId) {
		List<SubCategoryVO> subCategoryList = null;
		
		try {
			subCategoryList = searchDAO.getSubCategoryByCatId(catId);
		} catch (SQLException e) {
			LOG.error(e, e);
		}
		
		return subCategoryList;
	}
	
	/**
	 * 1차 카테고리 전체 목록 조회
	 * 
	 * @return
	 */
	public List<CategoryVO> selectAllCategory() {
		List<CategoryVO> categoryList = null;
		
		try {
			categoryList = searchDAO.selectAllCategory();
		} catch (SQLException e) {
			LOG.error(e, e);
		}
		
		return categoryList;
	}
	
	/**
	 * 1차, 2차 카테고리 전체 정보 조회
	 * 
	 * @return
	 */
	public List<HashMap<String, Object>> getCategoryInfoList() {
		// 1차 카테고리 전체 목록 조회
		List<CategoryVO> categoryList= selectAllCategory();
		if (CollectionUtils.isEmpty(categoryList)) {
			return null;
		}
		
		// 1차 카테고리 정보와 2차 카테고리 목록을 가지고 있는 목록 설정
		List<HashMap<String, Object>> categoryInfoList = new ArrayList<HashMap<String, Object>>();
		for (CategoryVO category : categoryList) {
			HashMap<String, Object> categoryInfoMap = new HashMap<String, Object>();
			int categoryId = category.getCategoryId();
			String categoryName = category.getName();
			categoryInfoMap.put("categoryId", categoryId);
			categoryInfoMap.put("categoryName", categoryName);
			
			List<SubCategoryVO> subCategoryList = getSubCategoryByCatId(categoryId);
			if (CollectionUtils.isNotEmpty(subCategoryList)) {
				categoryInfoMap.put("subCategoryList", subCategoryList);
			}
			
			categoryInfoList.add(categoryInfoMap);
		}
		
		return categoryInfoList;
	}
	
	/**
	 * 브랜드 전체 목록 조회
	 * 
	 * @return
	 */
	public List<BrandVO> getAllBrandList() {
		List<BrandVO> brandList = null;
		
		try {
			brandList = searchDAO.selectAllBrand();
		} catch (SQLException e) {
			LOG.error(e, e);
		}
		
		return brandList;
	}
	
	/**
	 * 네비게이션에 필요한 정보 조회
	 * 
	 * 1차, 2차 카테고리 정보 및 브랜드 정보
	 * 
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public Map<String, List> getNavigationInfo() {
		List<HashMap<String, Object>> categoryInfoList = getCategoryInfoList();
		List<BrandVO> brandList = getAllBrandList();
		
		Map<String, List> navigationInfo = new HashMap<String, List>();
		
		if (CollectionUtils.isNotEmpty(categoryInfoList)) {
			navigationInfo.put("categoryInfoList", categoryInfoList);
		}
		
		if (CollectionUtils.isNotEmpty(brandList)) {
			navigationInfo.put("brandList", brandList);
		}
		
		return navigationInfo;
	}
	
	public CosmeticVO getCosByCosId(int cosmeticsNo) {
		CosmeticVO cosmeticVO = null;
				
		try {
			cosmeticVO = searchDAO.selectCosByCosId(cosmeticsNo);
		} catch (SQLException e) {
		}
		
		return cosmeticVO;
	}

	//keyword로 search해주는 함수
	public List<Map<String, Object>>searchKeywordForReview(String searchKeyword) throws SQLException{
		return reviewBO.getProcessedReviewList(searchDAO.searchKeywordForReview(searchKeyword));
	}

	/*
	 * userNo로 유저가 보관한 리스트들을 가지고 온다.
	 */
	public List<Map<String, Object>> selectMyReviewByUserNo(int userNo) throws SQLException {
		List<Integer> reviewNoArray = new ArrayList<Integer>();
		List<ReviewVO> reviewList = new ArrayList<ReviewVO>();
		reviewNoArray = myReviewDAO.selectMyReviewByUserNo(userNo);//review번호 가져오기
		
		for(int i = 0; i < reviewNoArray.size(); i++) {
			reviewList.add(reviewBO.selectReviewByReviewNo(reviewNoArray.get(i)));//리뷰번호로 리뷰가져오기
		}
	
		
		return reviewBO.getProcessedReviewList(reviewList);
	}
	
}
