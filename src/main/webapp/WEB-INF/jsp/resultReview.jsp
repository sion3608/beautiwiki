<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="currentPageCss" value="common.css,resultReview.css" scope="request" />
<c:set var="title" value="resultReview" />
<%@include file="common/header.jsp"%>
</head>
<body>	
<div class="content-area">
	<div class="reviewRapper">
		<div class="triangle"></div>
		<span id ="title"> 
			${review.title}
		</span>
		<input type="hidden" name="reviewNo" id="reviewNumber" value="${review.reviewNo}"/>
		<c:if test="${not empty review.userNo}">
			<c:if test="${userNo != review.userNo}">
				<form action="addMyReview.do">
					<div>
						<input type="hidden" name="reviewNo" id="reviewNumber" value="${review.reviewNo}"/>
						<img class="zzim_img zzim" src="/static/img/zzim.jpg" width="60px;">
						<div id="zzim_txt" class="zzim">찜하기</div>
					</div>
				</form>
			</c:if>
		</c:if><br><hr style="margin: 0;margin-top:10px;margin-bottom:10px;">
		<span><fmt:formatDate value="${review.writeTime}" pattern="yyyy.MM.dd"></fmt:formatDate> |</span>&nbsp;&nbsp;
		<span>조회수</span>&nbsp;&nbsp;<span class="point">${review.hit} </span> |&nbsp;&nbsp; 
		<span>피부타입</span>&nbsp;&nbsp;<span class="point">${review.skinType} </span> |&nbsp;&nbsp;
		<span>추천수</span>&nbsp;&nbsp;<span class="point">${review.like} </span> |&nbsp;&nbsp;
		<span>비추천수</span>&nbsp;&nbsp;<span class="point">${review.disLike} </span>
		<div class="review_grade_area">
		 	&nbsp;&nbsp;추천의사
			<c:forEach var="grade" begin="1" end="5" step="1">
				<c:set var="tempGrade" value="${6 - grade}"/>
				<c:if test="${review.grade <= tempGrade}">
					<img style="float:left" class="grade_img" src="/static/img/star_on.png" width="18" height="18">
				</c:if>
				<c:if test="${review.grade > tempGrade}">
					<img style="float:left" class="grade_img" src="/static/img/star_off.png" width="20" height="18">
				</c:if>
			</c:forEach>
		</div>
		<div id="contents">
			${review.contents} 
		</div>
		<div class="isRecomm_area" style="margin-top : 50px;">
			<img style="margin:0 auto" class="up_img" src="/static/img/up.jpg" width="50" height="50">
			<img style="margin:0 auto" class="down_img" src="/static/img/down.jpg" width="50" height="50" >
		</div>
		<div class="isRecomm_area" style="margin-top : 10px;">
			<span class="up_img">추천</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<span class="down_img">비추천</span>
		</div>
	</div>
	
		<div class="commentRapper">
			<div class = "wrap" text-align = "center" style = "margin:0 auto;">
		<p>
			<h5><font color = "#5D5D5D">댓글 달기</font></h5>
			
			<form action = "insertComment.do" method = "post">
			<input type = "hidden" name = "reviewNo" value = "${review.reviewNo}">
			<input type = "hidden" name = "userNo" value = "${userNo}">
			<table>
				<tr>
					<td>
						<h6><font color = "grey">${userId}</font></h6>&nbsp;&nbsp;&nbsp;&nbsp;
					</td>
					<td width = "500" align = center>
						<textarea rows = "2" cols="70" name = comment value = "" placeholder = "댓글을 입력해주세요."></textarea>
					</td>
					<td>
						<input type = submit value = "댓글달기">
					</td>
			</table>
		</form>
		</p>
	</div>
</div>
<div class = "wrap" text-align = "center" style = "margin:0 auto;">
	<table border = 0>
		<c:forEach items="${commentlist}" var="comment" varStatus = "status">
			<div class = "line" text-align = "center" style = "width:50%; margin:0 auto;">
				<p>
				<c:choose>
					<c:when test = "${is eq 1 and commentNo eq comment.comment_no}">
					<form name = f method = post action = commentFix2.do>
						<input type = hidden name = "reviewNo" value = "${review.reviewNo}">
						<input type = hidden name = userNo value = "${userNo}">
						<input type = hidden name = commentNo value = "${comment.comment_no}">
						<tr>
		    				<td><font size = "2" color = "grey"><b>${comment.userId}</b></font></td>
						</tr>
						<tr>
							<td>
								<textarea rows = "2" cols="70" name = contents>${comment.contents}</textarea>
								<input type = submit value = "수정">
							</td>
						</tr>
						<tr>
							<td width = "600">
								<font size = "2" color = "#6799FF"><b>${comment.date}</b></font>
							</td>
						</tr>
						</form>
					</c:when>
					<c:otherwise>
						<tr>
							<td>
								<font size = "2" color = "grey"><b>${comment.userId}</b></font><br>
							</td>
						</tr>
						<tr>
							<td>
								<font color = "grey">${comment.contents}</font><br>
							</td>
						</tr>
						<tr>
							<td width = "600">
								<font size = "2" color = "grey"><b><font color = "#6799FF">${comment.date}</b></font>
							</td>
						<c:choose>
							<c:when test="${comment.userId eq userId}">
								<td>
									<form name = "f" method = post action = "commentFix.do">
										<input type = hidden name = "reviewNo" value = "${review.reviewNo}">
										<input type = hidden name = userNo value = "${userNo}">
										<input type = hidden name = commentNo value = "${comment.comment_no}">
										<input type = hidden name = "contents" value = "${comment.contents}">
										<input type = submit value = "수정">
									</form>
								</td>
								<td>
									<form name = "f2" method = post action = "deleteComment.do">
										<input type = hidden name = "reviewNo" value = "${reviewNo}">
										<input type = hidden name = commentNo value = "${comment.comment_no}">
										<input type = submit value = "삭제">
									</form>
								</td>
							</c:when>
						</c:choose>
						</tr>
						<tr>
							<td colspan = 3><hr size = "1" color = "grey"></td>
						</tr>
						</c:otherwise>	
					</c:choose>
				</p>
		</c:forEach>
	</table>
</div>
</div>
<%@include file="common/footer.jsp"%>
<script src="${JS_DIR}/jquery-1.10.1.js"></script>
<script src="${JS_DIR}/resultReview.js"></script>
<script language = "javaScript">
function fix(){
	f.action = "commentFix.do";
	f.submit();
}

function deleteC(){
	f.action = "deleteComment.do";
	f.submit();
}

</script>
<script type="text/javascript">
$(document).ready (function () {
	_bindEventLisener();
});
</script>