package com.crew91.beautiwiki.bo;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.crew91.beautiwiki.dao.MatchingDAO;
import com.crew91.beautiwiki.dao.UserDAO;
import com.crew91.beautiwiki.vo.CosmeticVO;
import com.crew91.beautiwiki.vo.ReviewVO;


@Component
public class MatchingBO {
	@Autowired
	private MatchingDAO matchingDAO;
	@Autowired
	private UserDAO userDAO; 
	@Autowired
	private List<ReviewVO> remainReviewList;
	@Autowired
	private SearchBO searchBO;
	
	private List<List<ReviewVO>> cosmeticsGroup;
	//dao에서 사용자 정보에 맞는 화장품과 리뷰번호를 가지고 와서 requisiteReviewList에 넣기
	public List<ReviewVO> requisiteReview(int userNO, int userAge) throws SQLException {
		List<ReviewVO> requisiteReviewList = null;
		requisiteReviewList = matchingDAO.remainReviewInfo(userNO, userAge);
		int count = 0;
		int flag = 0;
		for (int i = 0; i < requisiteReviewList.size(); i++) {
			flag = 0;
			for (int j = i + 1; j < requisiteReviewList.size(); j++) {
				if (requisiteReviewList.get(i).getCosmeticsNo() == requisiteReviewList.get(j).getCosmeticsNo()) {
					flag++;
				}
			}
			if (flag == 0) {
				count++;
			}
		}

		if (count < 4) {
			requisiteReviewList = matchingDAO.miniConditionMatching(userNO, userAge);
		}
		return requisiteReviewList;
	}

	public List<CosmeticVO> matchingReview(List<ReviewVO> remainReviewList) throws SQLException {
		//HashSet에 remainReviewList로 가져온 화장품번호와 리뷰번호 중에서 화장품번호의 중복제거해서 화장품번호 넣기
		HashSet<Integer> remainCosmeticsList = new HashSet<Integer> ();
		cosmeticsGroup = new ArrayList<List<ReviewVO>> ();
		for (int i = 0; i < remainReviewList.size(); i++) {
			ReviewVO reviewVO = remainReviewList.get(i);
			remainCosmeticsList.add(reviewVO.getCosmeticsNo());
		}
		//matchingDAO.selectedReviewInfo(i)를 통해서 리뷰번호와 화장품 번호, 추천여부 값을  matchingList에 넣고 
		//cosmeticsGroup에서 get(0)하면 만약 0이 846번이면, cosmeticsGroup.get(0)에 846번 화장품의 리뷰번호와 화장품 번호, 추천여부 값이 list로 들어가있음
		for (Integer i : remainCosmeticsList) {
			List<ReviewVO> matchingList = matchingDAO.selectedReviewInfo(i);
			cosmeticsGroup.add(matchingList);
		}
		//subAndCosmeticsList에 짝수번에 화장품번호, 홀수번에 추천-비추천한 값이 들어감
		ArrayList<Integer> subAndCosmeticsList = new ArrayList<Integer>();
		for (List<ReviewVO> list : cosmeticsGroup) {
			int up = 0;
			int down = 0;
			for (int i=0; i < list.size(); i++) {
				ReviewVO reviewVO = list.get(i);
				if (reviewVO.getIsRecomm() == 1) {
					up++;
				}
				else if (reviewVO.getIsRecomm() == 0) {
					down++;
				}
				if (i == 0) {
					subAndCosmeticsList.add(reviewVO.getCosmeticsNo());
				}
			}
			subAndCosmeticsList.add(up-down);
		}
		//selectTopthree의 짝수번에 화장품번호, 홀수번에 matchingDAO.calculatedGradeList한 결과 들어가도록
		ArrayList<Integer> selectTopthree = new ArrayList<Integer> ();
		for(int i = 0; i < subAndCosmeticsList.size(); i+=2) {
			selectTopthree.add(subAndCosmeticsList.get(i));
			int cGrade = matchingDAO.calculatedGradeList(subAndCosmeticsList.get(i), subAndCosmeticsList.get(i + 1));
			selectTopthree.add(cGrade);
		}
		//sum(grade) * (up-down)한 값 중에 값이 큰 3개의 값을 골라서 그 값들의 화장품 번호가 topThree배열에 들어가도록
		int[] topThree = new int[4];
		for (int i = 0; i < 4; i++) {
			int top = selectTopthree.get(1);
			int n = 0;
			for (int j = 0; j < selectTopthree.size(); j+=2) {
				if (selectTopthree.get(j + 1) > top) {
					top = selectTopthree.get(j + 1);
					n = j;
				}
			}
			topThree[i] = selectTopthree.get(n);
			for (int a = 0; a < 2; a++) {
				selectTopthree.remove(n);
			}		
		}
		for (int m = 0; m < 4; m++){
			System.out.println("화장품 번호: "+ topThree[m]);//사용자에게 맞는 top3화장품 번호가 topThree배열에 들어있음.
		}
		//5개 나오도록 하기, 화장품 정보나오도록(), 화장품 누르면 화장품 관련 리뷰나오도록(수정될 사항들)
		//matchingDAO.topThreeCosmeticsReviewInfo로 화장품번호와 관련된 모든 리뷰정보를 가져와서 topThreeList에 넣고
		//topThreeList를 cosmeticsGroup에 넣기
		cosmeticsGroup = new ArrayList<List<ReviewVO>> ();
		List<CosmeticVO> top3Cosmetics = new ArrayList<CosmeticVO>();
		for (int i = 0; i < topThree.length; i++) {
			CosmeticVO cosmeticVO = searchBO.getCosByCosId(topThree[i]);
			if (cosmeticVO != null) {
				top3Cosmetics.add(cosmeticVO);
			}
			
			//List<ReviewVO> topThreeList = matchingDAO.topThreeCosmeticsReviewInfo(topThree[i]);
			//cosmeticsGroup.add(topThreeList);
		}
		/*
		//각 화장품의 리뷰 리스트들 중에서 is_Recomm이 0인 리뷰 제거하고 likes-dislike한 값이 가장 높은 리뷰 1개 선택(선택된 롸장품에 관한 리뷰가 한개있을 경우 그 화장품 리뷰가 남아있도록 함)
		for (List<ReviewVO> list : cosmeticsGroup) {
			int max = list.get(0).getLike() - list.get(0).getDisLike();
			for (int i = 0; i < list.size(); i++) {
				ReviewVO reviewVO = list.get(i);
				if (reviewVO.getIsRecomm() == 0) {
					list.remove(i);
					i--;
				}
				else {
					if (list.size() != 1) {
						if (max < (reviewVO.getLike() - reviewVO.getDisLike())) {
							max = (reviewVO.getLike() - reviewVO.getDisLike());
						}
						else if (max >= (reviewVO.getLike() - reviewVO.getDisLike())) {
							list.remove(i);
							i--;
						}
					}
				}
			}
		}
		//matchingList에는 각 화장품의 리뷰정보들 중 가장 적합한 리뷰가 들어있음(위에서 선택한 총 3개 리뷰가 들어있음)
		List<ReviewVO> matchingList = new ArrayList<ReviewVO> ();
		for (List<ReviewVO> list : cosmeticsGroup) {
			for (int i=0; i < list.size(); i++) {
				ReviewVO reviewVO = list.get(i);
				matchingList.add(reviewVO);
			}
		}
		//이거는 제대로 값 나오는지 확인할려고 넣은 출력 과정
		System.out.println("------------------- 매칭 결과---------------------");
		for (ReviewVO list : matchingList) {
			System.out.println(list);
		}
		*/
		return top3Cosmetics;
	}

}
