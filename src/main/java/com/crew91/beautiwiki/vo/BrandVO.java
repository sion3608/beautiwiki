/*
 *  작성자 : 김 시온
 *  일자 : 2014.11.10일
 *  목적 : Brand 관련 vo
 */
package com.crew91.beautiwiki.vo;

import org.springframework.stereotype.Component;

@Component
public class BrandVO {
	private int brandId;
	private String name;
	
	public BrandVO() {}
	public BrandVO(int brandId, String name) {
		this.brandId = brandId;
		this.name = name;
	}
	public int getBrandId() {
		return brandId;
	}
	public void setBrandId(int brandId) {
		this.brandId = brandId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Override
	public String toString() {
		return "BrandVO [brandId=" + brandId + ", name=" + name + "]";
	}
}
