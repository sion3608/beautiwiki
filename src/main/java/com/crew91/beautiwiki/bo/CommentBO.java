package com.crew91.beautiwiki.bo;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;

import com.crew91.beautiwiki.dao.BrandDAO;
import com.crew91.beautiwiki.dao.CommentDAO;
import com.crew91.beautiwiki.dao.PreferDAO;
import com.crew91.beautiwiki.dao.ReviewDAO;
import com.crew91.beautiwiki.dao.UserDAO;
import com.crew91.beautiwiki.vo.BrandVO;
import com.crew91.beautiwiki.vo.CommentVO;
import com.crew91.beautiwiki.vo.PreferVO;
import com.crew91.beautiwiki.vo.UserVO;

@Component
public class CommentBO{
	@Autowired
	private CommentDAO commentDAO;
	
	/*커맨트를 인서트*/
	public boolean insertComment(int reviewNo, int userNo, String comment) {
		boolean result;
		result = commentDAO.insertComment(reviewNo, userNo, comment);
		System.out.println("inserResult : " + result);
		return result;
	}
	
	/*커맨트리스트를 얻어온다*/
	public List<CommentVO> getCommentList(int reviewNo) {
		System.out.print("commentBO" + reviewNo);
		return commentDAO.getCommentList(reviewNo);
	}
	
	/*커맨트를 수정한다.*/
	public boolean commentFix(int commentNo, String contents) {
	
		return commentDAO.commentFix(commentNo, contents);
	}
	
	/*커맨트를 삭재한다.*/
	public boolean deleteComment(int commentNo) {
		return commentDAO.deleteComment(commentNo);
	}
}
