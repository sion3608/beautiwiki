package com.crew91.beautiwiki.bo;

import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.crew91.beautiwiki.dao.MyReviewDAO;
import com.crew91.beautiwiki.vo.MyReviewVO;

@Component
public class MyReviewBO {

	@Autowired
	MyReviewDAO myReviewDAO;
	/*
	 * 목적: 리뷰를 보관하기전에 이 리뷰가 내 보관함에 있는지 없는지를 확인해준다.
	 * 파라미터 : 리뷰넘버와 유저넘버로 select
	 * 역할 : 리뷰가 있다면 -> 음수값 리턴
	 * 없다면 : insert 함
	 */
	public String validateMyReview(int reviewNo, int userNo) throws SQLException {

		MyReviewVO myReview = null;
		
		String resultMsg= "";
		
		if(myReviewDAO.selectMyReviewByReviewNoAndUserNO(reviewNo, userNo) >= 0) {
			myReview = new MyReviewVO(userNo, reviewNo);
			if(myReviewDAO.insertMyreviewByUserNoAndReviewNo(myReview) > 0) {
				 resultMsg =  "succInputMyReview";
			}//insert success 
			else {
				 resultMsg =  "failInputMyReview";
			} //insert fail
		} else {
			 resultMsg =  "isAlreayReview";
		} //review isalready db
		
		return resultMsg;
	}
}
