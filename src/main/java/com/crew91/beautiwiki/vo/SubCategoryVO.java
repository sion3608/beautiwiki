package com.crew91.beautiwiki.vo;

public class SubCategoryVO {

	private int subcategoryId;
	private int categoryId;
	private String name;
	private String descriptor;

	public SubCategoryVO() {
		super();
	}

	public SubCategoryVO(int subcategoryId, int categoryId, String name,
			String descriptor) {
		super();
		this.subcategoryId = subcategoryId;
		this.categoryId = categoryId;
		this.name = name;
		this.descriptor = descriptor;
	}

	public SubCategoryVO(int subcategoryId, String name) {
		super();
		this.subcategoryId = subcategoryId;
		this.name = name;
	}
	
	public int getSubcategoryId() {
		return subcategoryId;
	}


	public void setSubcategoryId(int subcategoryId) {
		this.subcategoryId = subcategoryId;
	}

	public int getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescriptor() {
		return descriptor;
	}

	public void setDescriptor(String descriptor) {
		this.descriptor = descriptor;
	}

	@Override
	public String toString() {
		return "SubCategoryVO [subcategoryId=" + subcategoryId
				+ ", categoryId=" + categoryId + ", name=" + name
				+ ", descriptor=" + descriptor + "]";
	}
	
	
}
