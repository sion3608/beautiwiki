package com.crew91.beautiwiki.dao;

import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.crew91.beautiwiki.vo.ReviewVO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

//2014.12.05 수정

@Component
public class MatchingDAO {
	@Autowired
	private DataSource ds;
	public MatchingDAO() throws Exception {
		Context init = new InitialContext();
		ds = (DataSource)init.lookup("java:comp/env/jdbc/OracleDB");
	}

	//사용자의 나이, 성별, 피부타입, 선호브랜드에 맞는 리뷰번호와 화장품 번호 추출
	public List<ReviewVO> remainReviewInfo(int userNo, int userAge) throws SQLException {
		Connection conn = null;
		PreparedStatement pstmt = null;
		
		ResultSet rs = null;
		List<ReviewVO> reviewList = new ArrayList<ReviewVO> ();
		try {
			StringBuffer findQuery = new StringBuffer();
			findQuery.append("select r.review_no as reviewNo, r.cosmetics_no as cosmeticsNo ");
			findQuery.append("from tb_recomm_age a JOIN (select * from tb_review where user_no != ?) r ON r.review_no = a.review_no ");
			findQuery.append("where (a.age <= ? and (a.age + 10) > ?) ");
			findQuery.append("and (r.cosmetics_no IN (select c.cosmetics_no from tb_cosmetics c ");
			findQuery.append("where c.sex IN (select sex from tb_user where user_no = ?) or c.sex = 2)) ");
			findQuery.append("and (r.skin_type = (select skin_type from tb_user where user_no = ?)) ");
			findQuery.append("and (r.cosmetics_no IN (select cosmetics_no from tb_cosmetics where brand_id ");
			findQuery.append("IN (select brand_id from tb_prefer where user_id = ?)))");
			conn = ds.getConnection();
			pstmt = conn.prepareStatement(findQuery.toString());
			pstmt.setInt(1, userNo);
			pstmt.setInt(2, userAge);
			if (userAge >= 50) {
				pstmt.setInt(3, 50);
			}
			else {
				pstmt.setInt(3, userAge);
			}
			pstmt.setInt(4, userNo);
			pstmt.setInt(5, userNo);
			pstmt.setInt(6, userNo);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				ReviewVO reviewVO = new ReviewVO(rs.getInt("reviewNo"), rs.getInt("cosmeticsNo"));
				reviewList.add(reviewVO);
			}

		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			if (pstmt != null) {
				pstmt.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
		return reviewList;
	}
	//화장품 번호에 맞는 모든 리뷰들의 리뷰번호와 화장품 번호, 추천여부 select
	public List<ReviewVO> selectedReviewInfo(int cosmeticsNo) throws SQLException {
		Connection conn = null;
		PreparedStatement pstmt = null;
		
		ResultSet rs = null;
		List<ReviewVO> matchingList = new ArrayList<ReviewVO> ();
		
		try {
			StringBuffer findQuery = new StringBuffer();
			findQuery.append("select review_no, cosmetics_no, is_recomm from tb_review where cosmetics_no=?");
			conn = ds.getConnection();
			pstmt = conn.prepareStatement(findQuery.toString());
			pstmt.setInt(1, cosmeticsNo);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				ReviewVO reviewVO = new ReviewVO(rs.getInt("review_no"), rs.getInt("cosmetics_no"), rs.getInt("is_recomm"));
				matchingList.add(reviewVO);
			}

		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			if (pstmt != null) {
				pstmt.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
		return matchingList;
	}
	//사용자에게 맞는 화장품 중 사용자에게 맞는 화장품 3개를 뽑기 위해서, 각 화장품 별 리뷰들에 있는 점수의 합과 ?(추천-비추천)을 곱한 값을 select
	public int calculatedGradeList(int cosmeticsNo, int subnum) throws SQLException {
		Connection conn = null;
		PreparedStatement pstmt = null;
		
		ResultSet rs = null;
		int cGrade = 0;

		try {
			StringBuffer findQuery = new StringBuffer();
			findQuery.append("select sum(grade)*? from tb_review where cosmetics_no = ?");
			conn = ds.getConnection();
			pstmt = conn.prepareStatement(findQuery.toString());
			pstmt.setInt(1, subnum);
			pstmt.setInt(2, cosmeticsNo);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				cGrade = rs.getInt(1); 
			}

		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			if (pstmt != null) {
				pstmt.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
		return cGrade;
	}
	//top3화장품에 대해 쓴 모든 리뷰정보들 추출
	public List<ReviewVO> topThreeCosmeticsReviewInfo(int cosmeticsNo) throws SQLException {
		Connection conn = null;
		PreparedStatement pstmt = null;
		
		ResultSet rs = null;
		List<ReviewVO> matchingList = new ArrayList<ReviewVO> ();
		
		try {
			StringBuffer findQuery = new StringBuffer();
			findQuery.append("select * from tb_review where cosmetics_no=?");
			conn = ds.getConnection();
			pstmt = conn.prepareStatement(findQuery.toString());
			pstmt.setInt(1, cosmeticsNo);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				ReviewVO reviewVO = new ReviewVO();
				reviewVO.setReviewNo(rs.getInt("review_no"));
				reviewVO.setUserNo(rs.getInt("user_no"));
				reviewVO.setCosmeticsNo(rs.getInt("cosmetics_no"));
				reviewVO.setGrade(rs.getInt("grade"));
				reviewVO.setIsRecomm(rs.getInt("is_recomm"));
				reviewVO.setSkinType(rs.getString("skin_type"));
				reviewVO.setWriteTime(rs.getTimestamp("write_time"));
				reviewVO.setHit(rs.getInt("hit"));
				reviewVO.setLike(rs.getInt("likes"));
				reviewVO.setDisLike(rs.getInt("dislikes"));
				reviewVO.setTitle(rs.getString("title"));
				reviewVO.setImgNames(rs.getString("img_names"));
				reviewVO.setContents(rs.getString("contents"));
				matchingList.add(reviewVO);
			}

		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			if (pstmt != null) {
				pstmt.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
		return matchingList;
	}
	//나이, 피부타입, 성별, 선호브랜드 중 최소한의 조건을 만족하는 리뷰가 있으면 리뷰번호와 화장품 번호 select
	public List<ReviewVO> miniConditionMatching(int userNo, int userAge) throws SQLException {
		Connection conn = null;
		PreparedStatement pstmt = null;
		
		ResultSet rs = null;
		List<ReviewVO> reviewList = new ArrayList<ReviewVO> ();
		try {

			StringBuffer findQuery = new StringBuffer();
			findQuery.append("select distinct r.review_no as reviewNo, r.cosmetics_no as cosmeticsNo ");
			findQuery.append("from tb_recomm_age a JOIN (select * from tb_review where user_no != ?) r ON r.review_no = a.review_no ");
			findQuery.append("where (a.age <= ? and (a.age + 10) > ?) ");
			findQuery.append("or (r.cosmetics_no IN (select c.cosmetics_no from tb_cosmetics c ");
			findQuery.append("where c.sex IN (select sex from tb_user where user_no = ?) or c.sex = 2)) ");
			findQuery.append("or (r.skin_type = (select skin_type from tb_user where user_no=?)) ");
			findQuery.append("or (r.cosmetics_no IN (select cosmetics_no from tb_cosmetics where brand_id ");
			findQuery.append("IN (select brand_id from tb_prefer where user_id = ?)))");
			conn = ds.getConnection();
			pstmt = conn.prepareStatement(findQuery.toString());
			pstmt.setInt(1, userNo);
			pstmt.setInt(2, userAge);
			if (userAge >= 50) {
				pstmt.setInt(3, 50);
			}
			else {
				pstmt.setInt(3, userAge);
			}
			pstmt.setInt(4, userNo);
			pstmt.setInt(5, userNo);
			pstmt.setInt(6, userNo);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				ReviewVO reviewVO = new ReviewVO(rs.getInt("reviewNo"), rs.getInt("cosmeticsNo"));
				reviewList.add(reviewVO);
			}

		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			if (pstmt != null) {
				pstmt.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
		return reviewList;
	}
	
}
