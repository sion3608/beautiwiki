<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="EUC-KR"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<script>
function updateUser(){
		document.f.action = "updateUser1.do";
		f.submit();
	}
function deleteUser(){
	document.f.action = "deleteUser.do";
	f.submit();
}
</script>

<c:set var="currentPageCss" value="main.css" scope="request" />
<jsp:include page="/WEB-INF/jsp/common/header.jsp" />

<section id = "container">
	<div class="wrap" text-align = center style="width:30%; margin:0 auto;">
		<br><br>
		<h2><font color = #DB4A37>회원정보</font></h2>
		<p>&nbsp;&nbsp;&nbsp;<br></p>
		<form name = f method = post>
		<input type = hidden name = "upNo" value = "2">
		<input type = hidden name = "userId" value = "${userInfo.userId}">
		<input type = hidden name = "userNo" value = "${userInfo.userNo}">
		<p> 
			<table border = 0 width = 500>
				<tr height = 50>
					<td><font color = grey>아이디</font></td>
					<td align = center>${userInfo.userId}</td>
				</tr>
				<tr height = 50>
					<td><font color = grey>비밀번호</font></td>
					<td align = center>${userInfo.pw}</td>
				</tr>
				<tr height = 50>
					<td><font color = grey>이름</font></td>
					<td align = center>${userInfo.userName}</td>
				</tr>
				<tr height = 50>
					<td><font color = grey>나이</font></td>
					<td align = center>${userInfo.age}</td>
				</tr>
				<tr height = 50>
					<td><font color = grey>성별</font></td>
					<td align = center>			
						<c:choose>
					    <c:when test="${userInfo.sex eq '1'}">
			    		여자
				    	</c:when>
						<c:otherwise>
				       	남자
				        </c:otherwise>
						</c:choose>
					</td>
				</tr>
				<tr height = 50>
					<td><font color = grey>스킨타입</font></td>
					<td align = center>${userInfo.skinType}</td>
				</tr>
				<tr height = 50>
					<td><font color = grey>선호회사제품</font></td>
					<td align = center>
						<c:forEach items="${userPrefer}" var="prefers" varStatus = "status">
							${prefers.name}
							<c:if test = "${status.count%4 eq 0}"><br></c:if>
					</c:forEach>
					</td>
				</tr>
			</table>
		</p>
		<p class="input_btn_area">
			<input type = button class="input_btn" value = "수정하기" onClick = "updateUser()">
		</p>
		<p class="input_btn_area">
			<input type = button class="input_btn" value = "탈퇴하기" onClick = "deleteUser()">
		</p>
		<p class="input_btn_area">
			<input type = button class="input_btn" value = "메인으로" onClick = "history.go(-1)">
		</p>
	</form>
	</div>
</section>

<%--임시 --%>
<form action = "goReview.do"><%--임시임 --%>
임시리뷰페이지가기 <input type = text name = id><input type = submit value = "가기">
</form>
<jsp:include page="/WEB-INF/jsp/common/footer.jsp" />
</body>
</html>