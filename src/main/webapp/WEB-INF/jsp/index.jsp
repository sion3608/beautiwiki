<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<% session.invalidate(); %>
<script language = "javaScript">
function signUp(){
	f.action = "signUp.do";
	f.submit();
}
function Login(){
	document.f.action="login.do";
	f.submit();
}
</script>

<c:set var="currentPageCss" value="main.css" scope="request" />
<!doctype html>
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>${title}</title>
<c:set var="JS_DIR" value="static/js" scope="request"/>
<c:set var="CSS_DIR" value="static/css" scope="request"/>
<c:set var="IMG_DIR" value="static/img" scope="request"/>
<link rel="stylesheet" href="${CSS_DIR}/common.css" />
<c:if test="${not empty currentPageCss}">
	<c:forTokens items="${currentPageCss}" delims="," var="css">	
		<link rel="stylesheet" type="text/css" href="${CSS_DIR}/${fn:trim(css)}" />
	</c:forTokens>
</c:if>
<script type="text/javascript" src="${JS_DIR}/jindo.desktop.ns.js"></script>
<script type="text/javascript" src="${JS_DIR}/jindo_component.js"></script>
<script type="text/javascript" src="${JS_DIR}/jindo_mobile_component.js"></script>
</head>
<body style="background-image:url('/static/img/index_dark.png');background-repeat:none;">


<section id="container">
	<div class="login">
		<div class="logo_section" style="color:#ffffff;text-align:center;font-size:40px;font-weight:bold;margin-top:100px;margin-bottom:30px;">beautiwiki</div>
		<div class="wrap">
			<form name = f method="post" action="login.do">
				<p>
					<input type="text" class="input_row2" name="userId" value="" placeholder="username or e-mail">
				</p>
				<p>
					<input type="password" class="input_row2" name="password" value="" placeholder="password">
				</p>
				<p class="input_btn_area2" style="clear:both" onclick="f.submit()">
					로그인
				</p>
				<p class="input_btn_area2" style="clear:both" onclick="signUp()">
					가입
				</p>
			</form>
		</div>
		<span class="copyright">BEAUTIWIKI Copyright ©crew91. All Rights Reserved.</span>
	</div>
</section>

<c:choose> <%--컨트롤러의 반환값에 따라 경고창이 달라진다. --%>
	<c:when test="${is eq -1}">
		<script language = javascript> alert("아이디가 존재하지 않습니다.");</script>
	</c:when>
	<c:when test="${is eq 0}">
		<script language = javascript> alert("비밀번호가 다릅니다.");</script>
	</c:when>
	<c:when test="${is eq 2}">
		<script language = javascript> alert("탈퇴하셨습니다.");</script>
	</c:when>
	<c:when test="${is eq 3}">
		<script language = javascript> alert("아이디를 입력하세요.");</script>
	</c:when>
	<c:when test = "${is eq 4}">
		<script language = javascript> alert("패스워드를 입력하세요.");</script>
	</c:when>
	<c:when test = "${is eq 5}">
		<script language = javascript> alert("로그아웃 하셨습니다.");</script>
	</c:when>
</c:choose>

</body>
</html>
<script type="text/javascript" src="${JS_DIR}/common.js"></script>
<script type="text/javascript">
	var oCommon = new Common();
</script>

