package com.crew91.beautiwiki.controller;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import com.crew91.beautiwiki.bo.MatchingBO;
import com.crew91.beautiwiki.bo.SearchBO;
import com.crew91.beautiwiki.bo.UserBO;
import com.crew91.beautiwiki.dao.BrandDAO;
import com.crew91.beautiwiki.dao.UserDAO;
import com.crew91.beautiwiki.vo.BrandVO;
import com.crew91.beautiwiki.vo.UserVO;

@Controller
@SessionAttributes({"userNo", "userId"})
public class UserController {
	@Autowired
	private BrandDAO brandDAO;
	
	@Autowired
	private UserBO userBO;
	@Autowired
	private UserDAO userDAO;
	@Autowired
	private MatchingBO matchingBO;
	@Autowired
	private SearchBO searchBO;
	
	@RequestMapping(value="/index.do")
	public String showIndex(Model model) {
		return "index";
	}
	
	@RequestMapping(value="/login.do")
	public String login(Model model, @RequestParam("userId") String userId, @RequestParam("password") String password) {
		System.out.println("controller" + userId + " " + password);
		UserVO uservo = userBO.login(userId, password);
		String returnPage;
		if(userId == "" || password == "") {
			if(userId == "") 
				model.addAttribute("is", 3);
			else
				model.addAttribute("is", 4);
			returnPage = "index";
		}
		
		if(uservo == null){
			model.addAttribute("is", -1);
			returnPage = "index";
		}else if(uservo.getUserId().equals(userId) && uservo.getPw().equals(password)) {
			model.addAttribute("user", uservo);
			model.addAttribute("userNo", uservo.getUserNo());
			model.addAttribute("userId", uservo.getUserId());
			
			returnPage = "redirect:/home.do";
		} else {
			model.addAttribute("is", 0);
			returnPage = "index";
		}		
		return returnPage;
	}
	/*
	@RequestMapping(value="/main.do" )
	public String showMain(Model model, @RequestParam(value= "userId") String userId) {
		System.out.println("userId:" + userId);
		try {
			UserVO userVO = userDAO.selectUserInfo(userId);//매칭 수정된 부분
			System.out.println(userVO.getUserNo());
			System.out.println(userVO.getAge());
			List<ReviewVO> remainReviewList = matchingBO.requisiteReview(userVO.getUserNo(), userVO.getAge());//매칭
			//resultMatching에 매칭 결과로 나온 리뷰 정보들 들어가 있어
			List<ReviewVO> resultMatching = matchingBO.matchingReview(remainReviewList); 
			model.addAttribute("matchingList", resultMatching);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "main";
	}
	*/
	@RequestMapping(value="/updateUser1.do")
	public String updateUser1(Model model, Model model2, @RequestParam("userId")String userId, @RequestParam("upNo")int upNo) {
		System.out.println(userId);
		UserVO userinfo = new UserVO();
		List<BrandVO> userprefer = new ArrayList<BrandVO>();
		List<BrandVO> brandList = new ArrayList<BrandVO>();
	
		userinfo = userBO.updateUser1(userId);

		if(upNo == 1) {
			userprefer = userBO.getPrefer(userinfo.getUserNo());
			model.addAttribute("userInfo", userinfo);
			model2.addAttribute("userPrefer", userprefer);
			// 네비게이션에 필요한 정보 조회
			model.addAttribute("navigationInfo", searchBO.getNavigationInfo());
			return "updateUser1";
		}
		else if(upNo == 2) {
			brandList = userBO.getBrandList();
			userprefer = userBO.getPrefer(userinfo.getUserNo());
		
			model.addAttribute("userInfo", userinfo);
			model2.addAttribute("brandList", brandList);
			// 네비게이션에 필요한 정보 조회
			model.addAttribute("navigationInfo", searchBO.getNavigationInfo());
			return "updateUser2";
		}
		else
			return "error";
	}

	@RequestMapping(value="/updateUser2.do")
	public String updateUser2(Model model, Model model2, @RequestParam("userNo")int userNo, @RequestParam("userId") String userId, @RequestParam("password") String pw, @RequestParam("userName") String userName, @RequestParam("sex") int sex, @RequestParam("age") int age, @RequestParam("skinType") String skinType, @RequestParam("prefer") int[] prefer) {
		UserVO userinfo = new UserVO(userNo, userId, pw, sex, age, skinType, userName);
		List<BrandVO> userprefer = new ArrayList<BrandVO>();
		
		userBO.updateUser2(userinfo, prefer);
		userprefer = userBO.getPrefer(userinfo.getUserNo());
		System.out.println(age);
		model.addAttribute("userInfo", userinfo);
		model2.addAttribute("userPrefer", userprefer);
		// 네비게이션에 필요한 정보 조회
		model.addAttribute("navigationInfo", searchBO.getNavigationInfo());
		return "updateUser1";
	}
	
	@RequestMapping(value="/deleteUser.do")
	public String deleteUser(Model model, @RequestParam("userNo")int userNo) {
		try {
			userBO.deleteUser(userNo);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		model.addAttribute("is", 2);
		return "index";
	}
	
	@RequestMapping(value="/logout.do")
	public String logout(Model model, SessionStatus session ) {
		model.addAttribute("is", 5);
		session.setComplete();
		
		return "index";
	}
	
	@RequestMapping(value="/signUp.do")
	public String showRegister(Model model) {
		List<BrandVO> brandList = null; 
		 try {
			brandList = brandDAO.selectAllBrand();
			model.addAttribute("allBrandList", brandList);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		return "signUp";
	}
	
	@RequestMapping(value="/signUpService.do")
	public String showLogin(Model model, @RequestParam(value= "userId") String userId, 
			@RequestParam(value= "password") String password, @RequestParam(value= "password2") String password2,
			@RequestParam(value= "userName") String userName, @RequestParam(value= "age") int age,
			@RequestParam(value= "sex") int sex, @RequestParam(value= "skinType") String skinType, 
			HttpServletRequest request) {

		String[] brand = request.getParameterValues("brandName");
		
		for(int i = 0; i < brand.length; i++) {
			System.out.println(brand[i]);
		}
		String returnPage = "index";
		if (!(password.equals(password2))) {
			model.addAttribute("is", 6);
			List<BrandVO> brandList = null; 
			 try {
				brandList = brandDAO.selectAllBrand();
				model.addAttribute("allBrandList", brandList);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			returnPage = "signUp";
		}
		else if (password.equals(password2)) {
			try {
				userBO.signUpService(userBO.makeUser(userId, password, password2, userName, age, sex, skinType), brand);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			returnPage = "index";
		}
		return returnPage;
	}
	
}
