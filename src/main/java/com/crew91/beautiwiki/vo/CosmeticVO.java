package com.crew91.beautiwiki.vo;

public class CosmeticVO {
		
	private int cosmeticsNo;
	private int brandId;
	private int subCatId;
	private String name;
	private int price;
	private int sex;
	private String imgUrl;
	private int volume;
	private String brandName;
	
	public CosmeticVO() {
		super();
	}
	public CosmeticVO(int cosmeticsNo, int brandId, int subCatId, String name, int price,
			int sex, String imgUrl, int volume) {
		super();
		this.cosmeticsNo = cosmeticsNo;
		this.brandId = brandId;
		this.subCatId = subCatId;
		this.name = name;
		this.price = price;
		this.sex = sex;
		this.imgUrl = imgUrl;
		this.volume = volume;
	}
	public int getCosmeticsNo() {
		return cosmeticsNo;
	}
	public void setCosmeticsNo(int cosmeticsNo) {
		this.cosmeticsNo = cosmeticsNo;
	}
	public int getBrandId() {
		return brandId;
	}
	public void setBrandId(int brandId) {
		this.brandId = brandId;
	}
	public int getSubCatId() {
		return subCatId;
	}
	public void setSubCatId(int subCatId) {
		this.subCatId = subCatId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public int getSex() {
		return sex;
	}
	public void setSex(int sex) {
		this.sex = sex;
	}
	public String getImgUrl() {
		return imgUrl;
	}
	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}
	public int getVolume() {
		return volume;
	}
	public void setVolume(int volume) {
		this.volume = volume;
	}
	@Override
	public String toString() {
		return "CosmeticsVO [cosmeticsNo=" + cosmeticsNo + ", brandId="
				+ brandId + ", subCatId=" + subCatId + ", name=" + name
				+ ", sex=" + sex + ", imgUrl=" + imgUrl + ", volume=" + volume
				+ "]";
	}
	public String getBrandName() {
		return brandName;
	}
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

}
