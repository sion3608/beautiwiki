package com.crew91.beautiwiki.vo;

import org.springframework.stereotype.Component;
@Component
public class MyReviewVO {
	private int myReviewNo;
	private int userNo;
	private int reviewNo;
	
	
	public MyReviewVO(){}
	public MyReviewVO(int userNo, int reviewNo) {
		super();
		this.userNo = userNo;
		this.reviewNo = reviewNo;
	}
	
	public MyReviewVO(int myReviewNo,int userNo, int reviewNo) {
		super();
		this.myReviewNo = myReviewNo;
		this.userNo = userNo;
		this.reviewNo = reviewNo;
	}
	
	public int getMyReviewNo() {
		return myReviewNo;
	}
	public void setMyReviewNo(int myReviewNo) {
		this.myReviewNo = myReviewNo;
	}
	public int getUserNo() {
		return userNo;
	}
	public void setUserNo(int userNo) {
		this.userNo = userNo;
	}
	public int getReviewNo() {
		return reviewNo;
	}
	public void setReviewNo(int reviewNo) {
		this.reviewNo = reviewNo;
	}

	
	@Override
	public String toString() {
		return "MyReviewVo [userNo=" + userNo+ ", reviewNo=" + reviewNo
				+ ", myReviewNo=" + myReviewNo + ", toString()="
				+ super.toString() + "]";
	}
}
