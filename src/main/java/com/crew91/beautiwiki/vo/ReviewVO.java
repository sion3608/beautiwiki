/*
 *  작성자 : 김 시온
 *  일자 : 2014.11.10일
 *  목적 : review 관련 vo
 */
package com.crew91.beautiwiki.vo;

import java.util.Date;

import org.springframework.stereotype.Component;

@Component
public class ReviewVO {
	private int reviewNo;
	private int userNo;
	private int cosmeticsNo;
	private String title;
	private String contents;
	private int grade;
	private int isRecomm;
	private String skinType;
	private Date writeTime;
	private int hit;
	private int like;
	private int disLike;
	private String imgNames;
	
	public ReviewVO(){}
	
	public ReviewVO(int reviewNo, int cosmeticsNo) {
		super();
		this.reviewNo = reviewNo;
		this.cosmeticsNo = cosmeticsNo;
		this.hit = 0;
		this.like = 0;
		this.disLike = 0;
	}
	
	public ReviewVO(int reviewNo, int cosmeticsNo, int isRecomm) {
		super();
		this.reviewNo = reviewNo;
		this.cosmeticsNo = cosmeticsNo;
		this.isRecomm = isRecomm;
		this.hit = 0;
		this.like = 0;
		this.disLike = 0;
	}
	
	public ReviewVO( int userNo, int cosmeticsNo, String title, String contents, int grade,
			int isRecomm, String skinType, String imgNames) {
		super();
		this.userNo = userNo;
		this.cosmeticsNo = cosmeticsNo;
		this.title = title;
		this.contents = contents;
		this.grade = grade;
		this.isRecomm = isRecomm;
		this.skinType = skinType;
		this.imgNames = imgNames;
		this.hit = 0;
		this.like = 0;
		this.disLike = 0;
	}

	public ReviewVO(int reviewNo, int userNo, int cosmeticsNo, String contents,
			int grade, int isRecomm, String skinType, Date writeTime, int hit,
			int like, int disLike,  String imgNames) {
		super();
		this.reviewNo = reviewNo;
		this.userNo = userNo;
		this.cosmeticsNo = cosmeticsNo;
		this.contents = contents;
		this.grade = grade;
		this.isRecomm = isRecomm;
		this.skinType = skinType;
		this.writeTime = writeTime;
		this.hit = hit;
		this.like = like;
		this.disLike = disLike;
		this.imgNames = imgNames;
	}
	
	public int getReviewNo() {
		return reviewNo;
	}


	public void setReviewNo(int reviewNo) {
		this.reviewNo = reviewNo;
	}


	public int getUserNo() {
		return userNo;
	}
	public void setUserNo(int userNo) {
		this.userNo = userNo;
	}
	public int getCosmeticsNo() {
		return cosmeticsNo;
	}
	public void setCosmeticsNo(int cosmeticsNo) {
		this.cosmeticsNo = cosmeticsNo;
	}
	
	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public String getContents() {
		return contents;
	}
	public void setContents(String contents) {
		this.contents = contents;
	}
	public int getGrade() {
		return grade;
	}
	public void setGrade(int grade) {
		this.grade = grade;
	}
	public int getIsRecomm() {
		return isRecomm;
	}
	public void setIsRecomm(int isRecomm) {
		this.isRecomm = isRecomm;
	}
	public String getSkinType() {
		return skinType;
	}
	public void setSkinType(String skinType) {
		this.skinType = skinType;
	}
	public Date getWriteTime() {
		return writeTime;
	}
	public void setWriteTime(Date writeTime) {
		this.writeTime = writeTime;
	}
	public int getHit() {
		return hit;
	}
	public void setHit(int hit) {
		this.hit = hit;
	}
	public int getLike() {
		return like;
	}
	public void setLike(int like) {
		this.like = like;
	}
	public int getDisLike() {
		return disLike;
	}
	public void setDisLike(int disLike) {
		this.disLike = disLike;
	}


	
	public String getImgNames() {
		return imgNames;
	}


	public void setImgNames(String imgNames) {
		this.imgNames = imgNames;
	}


	@Override
	public String toString() {
		return "ReviewVO [reviewNo=" + reviewNo + ", userNo=" + userNo
				+ ", cosmeticsNo=" + cosmeticsNo + ", title=" + title
				+ ", contents=" + contents + ", grade=" + grade + ", isRecomm="
				+ isRecomm + ", skinType=" + skinType + ", writeTime="
				+ writeTime + ", hit=" + hit + ", like=" + like + ", disLike="
				+ disLike + ", imgNames=" + imgNames + "]";
	}
	
		
}
