package com.crew91.beautiwiki.bo;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.crew91.beautiwiki.dao.MyReviewDAO;
import com.crew91.beautiwiki.dao.RecommAgeDAO;
import com.crew91.beautiwiki.dao.ReviewDAO;
import com.crew91.beautiwiki.vo.CosmeticVO;
import com.crew91.beautiwiki.vo.MyReviewVO;
import com.crew91.beautiwiki.vo.RecommAgeVO;
import com.crew91.beautiwiki.vo.ReviewVO;

@Component
public class ReviewBO {
	public static final String imgPrePath = "/editor/upload/";
	public static final String defaultImgPrePath = "/editor/cosmetics/";
	
	@Autowired
	ReviewDAO reviewDAO;
	@Autowired
	RecommAgeDAO recommAgeDAO;
	@Autowired
	MyReviewDAO myreviewDAO;
	@Autowired
	SearchBO searchBO;

	/*리뷰작성한 요소들 디비에 저장해주는 로직수행
	반환값 : 리뷰테이블 insert 성공 -> recommAgee테이블 성공 -> 1반환
																	실패 -> -1반환
			 리뷰테이블  insert 실패 -> 0반환*/
	public int createReview(int userNo, String textAreaContent, int cosId, String title,
			String skinType, int isRecomm, int grade, String[] generation,
			String imgNames) {

		int result = 0;
		ReviewVO reviewVO = new ReviewVO(userNo, cosId, title, textAreaContent,
				grade, isRecomm, skinType, imgNames);
		result = reviewDAO.insertReview(reviewVO);

		if (result == 0) {
			return result;
		} else {
			List<RecommAgeVO> recommAgeList = new ArrayList<RecommAgeVO>();
			RecommAgeVO recommAgeVO = null;

			int reviewNo = reviewVO.getReviewNo();
			for (int i = 0; i < generation.length; i++) {
				recommAgeVO = new RecommAgeVO(reviewNo,
						Integer.parseInt(generation[i]));
				recommAgeList.add(recommAgeVO);
			}
			
			recommAgeDAO.insertRecommAgeByReviewNO(recommAgeList);

			return result; //insert된 리뷰넘버반환
		} // insert
	}
	
	/**
	 * 1차 카테고리 아이디, 2차 카테고리 아이디로 리뷰 목록 조회
	 * 
	 * @param categoryId
	 * @param subcategoryId
	 * @return
	 */
	public List<Map<String, Object>> getProcessedReviewListByCategoryIdAndSubcategoryId(int categoryId, int subcategoryId, int page, int countPerPage) {
		// 리뷰 목록을 가져온다.
		List<ReviewVO> reviewList = null;
		try {
			reviewList = reviewDAO.getReviewByCategoryIdAndSubcategoryIdPerPage(categoryId, subcategoryId, page, countPerPage);
		} catch (SQLException e) {
		}
		
		if (CollectionUtils.isEmpty(reviewList)) {
			return null;
		}
		
		// 이미지 URL을 가공한 리뷰 정보를 가진 리스트를 리턴한다.
		return getProcessedReviewList(reviewList);
	}
	
	/**
	 * 브랜드 아이디로 리뷰 목록 조회
	 * 
	 * @param brandId
	 * @return
	 */
	public List<Map<String, Object>> getProcessedReviewListByBrandId(int brandId, int page, int countPerPage) {
		// 리뷰 목록을 가져온다.
		List<ReviewVO> reviewList = null;
		try {
			reviewList = reviewDAO.getReviewByBrandIdPerPage(brandId, page, countPerPage);
		} catch (SQLException e) {
		}
		
		if (CollectionUtils.isEmpty(reviewList)) {
			return null;
		}
		
		// 이미지 URL을 가공한 리뷰 정보를 가진 리스트를 리턴한다.
		return getProcessedReviewList(reviewList);
	}
	
	/**
	 * 리뷰 목록을 DB에서 가져와서 가공한다.
	 * 
	 * ("review" : reviewVO, "imgUrlList" : 이미지 URL 목록) 
	 * 
	 * @param reviewList
	 * @return
	 */
	public List<Map<String, Object>> getProcessedReviewList(List<ReviewVO> reviewList) {
		List<Map<String, Object>> processedReviewList = new ArrayList<Map<String, Object>>();
		for (ReviewVO review : reviewList) {
			if (review == null) {
				continue;
			}
			
			Map<String, Object> reviewInfo = new HashMap<String, Object>();
			
			// 리뷰 VO 정보
			reviewInfo.put("review", review);
			
			// 이미지 URL 경로 목록
			List<String> imgUrlList = getImgUrlList(review.getImgNames());
			if (CollectionUtils.isEmpty(imgUrlList)) {
				// 화장품 디폴트 사진 연결하기
				CosmeticVO cosmeticVO = searchBO.getCosByCosId(review.getCosmeticsNo());
				imgUrlList = new ArrayList<String>();
				imgUrlList.add(defaultImgPrePath + StringUtils.trimWhitespace(cosmeticVO.getName()) + ".JPG");
			}
			reviewInfo.put("imgUrlList", imgUrlList);
			processedReviewList.add(reviewInfo);
		}
		
		return processedReviewList;
	}
	
	/**
	 * xxx.jpg|xxx.png|xxx.jpg (imgNames)를 가지고 이미지 URL들을 저장한 리스트를 만들어 리턴한다.
	 * 
	 * @param imgNames
	 * @return
	 */
	private List<String> getImgUrlList(String imgNames) {
		if (StringUtils.isEmpty(imgNames)) {
			return null;
		}
		
		List<String> imgUrlList = new ArrayList<String>();
		
		boolean hasOneImgName = !imgNames.contains("|");
		if (!hasOneImgName) {
			String[] imgNameArray = StringUtils.split(imgNames, "|");
			if (imgNameArray == null || imgNameArray.length < 1) {
				return null;
			}
			
			int countOfImgName = imgNameArray.length;
			for (int i = 0; i < countOfImgName; i++) {
				String imgName = imgNameArray[i];
				if (StringUtils.isEmpty(imgName)) {
					continue;
				}
				
				imgUrlList.add(imgPrePath + imgName);
			}
		} else {
			imgUrlList.add(imgPrePath + imgNames);
		}
		
		return imgUrlList;
	}
	
	//최신리뷰 선택
	public List<Map<String, Object>> newestReviewList() {
		List<ReviewVO> newestReviewList = null;;
		try {
			newestReviewList = reviewDAO.newestReviewList();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(newestReviewList.size());
		return getProcessedReviewList(newestReviewList);
	}
	//hit수 높은 리뷰 선택
	public List<Map<String, Object>> highHitReviewList() {
		List<ReviewVO> highHitReviewList = null;
		try {
			highHitReviewList = reviewDAO.highHitReview();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(highHitReviewList.size());
		return getProcessedReviewList(highHitReviewList);
	}

	/*
	 * 작성자 : 김시온
	 * params : reviewNo(리뷰번호)
	 * 목적 : reviewList에서 review클릭시 reviewNo로 해당 리뷰 가지고 오는 메소드
	 */
	public ReviewVO selectReviewByReviewNo(int reviewNo) throws SQLException {
		return reviewDAO.selectReviewByReviewNo(reviewNo);
	}
	
	public int insertMyreviewByUserNoAndReviewNo(MyReviewVO myReviewVO) throws SQLException{
		return myreviewDAO.insertMyreviewByUserNoAndReviewNo(myReviewVO);
	}

	public int updateHitNumber(int reviewNo) throws SQLException{
		return reviewDAO.updateHitNumber(reviewNo);
	}
	
	public int updateLikeNumber(int reviewNo) throws SQLException{
		return reviewDAO.updateLikeNumber(reviewNo);
	}
	
	public int updateDisLikeNumber(int reviewNo) throws SQLException{
		return reviewDAO.updateLikeNumber(reviewNo);
	}

	public List<Map<String, Object>> selectReviewBycosId(int cosmeticsNo)throws SQLException {
		return getProcessedReviewList(reviewDAO.selectReviewBycosId(cosmeticsNo));
	}
}
