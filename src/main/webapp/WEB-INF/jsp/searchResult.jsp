<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="currentPageCss" value="searchResult.css,reviewList.css" scope="request" />
<c:set var="title" value="searchResult" />
<%@include file="common/header.jsp"%>
</head>
<body>	
<div class="title">
	<c:if test="${not empty flagMsg && flagMsg eq 'myReview'}">
		<div id="searchTitle" style="color:#CE483D;margin-top: 50px;font-size: 25px;font-weight: bold;margin-left:5px;margin-bottom:8px;}">리뷰보관함</div>
	</c:if>
	<c:if test="${empty flagMsg}">
		<div id="searchTitle" style="color:#CE483D;margin-top: 50px;font-size: 25px;font-weight: bold;margin-left:5px;margin-bottom:8px;}">리뷰가 없습니다. 추가해주세요.</div>
	</c:if>
	<c:if test="${not empty flagMsg && flagMsg eq 'search'}">
		<div id="searchTitle" style="color:#CE483D;margin-top: 50px;font-size: 25px;font-weight: bold;margin-left:5px;margin-bottom:8px;}">검색결과</div>
	</c:if>
</div>	
<div>
		<ul id="tiles2">
			<c:if test="${not empty processedReviewList}">
				<c:forEach var="processedReview" items="${processedReviewList}" varStatus="sequence">
					<li style="float:left;"<c:if test="${not empty processedReview.review}"> class="clickReviewNo _params('${processedReview.review.reviewNo}')"</c:if>>
						<div style="width: 220px;background-color: #ffffff;border: 1px solid;border-color: #e9eaed #dfe0e4 #d0d1d5;border-radius: 2px;-moz-border-radius: 2px;-webkit-border-radius: 2px;cursor: pointer;border-radius: 0.2em;">
							<c:if test="${not empty processedReview.review}">
								<div class="_clickable_area">
									<c:if test="${not empty processedReview.imgUrlList}">
										<c:forEach var="imgUrl" items="${processedReview.imgUrlList}" varStatus="sequence">
											<c:if test="${sequence.count eq 1}">
												<div class="review_img_area">
													<img class="review_img" src="${imgUrl}" width="220" height="300">
												</div>
											</c:if>
										</c:forEach>
									</c:if>
									
									<div class="review_content_area">
										<div class="review_recomm_area">
											<c:if test="${processedReview.review.isRecomm eq 1}">
												<img class="recommend_img" src="/static/img/smile.png" width="20" height="20">
											</c:if>
											<c:if test="${processedReview.review.isRecomm eq 0}">
												<img class="recommend_img" src="/static/img/sad.png" width="20" height="20">
											</c:if>
										</div>
										
										<div class="review_title_area" style="height:20px;overflow:hidden;">
											${processedReview.review.title}
										</div>
										
										<div class="review_hit_and_writedate_area">
											조회수 <span class="point">${processedReview.review.hit}</span> <span class="division">|</span> <fmt:formatDate value="${processedReview.review.writeTime}" pattern="yyyy.MM.dd"></fmt:formatDate>
										</div>
									</div>
									
									<div class="review_grade_area">
										<c:forEach var="grade" begin="1" end="5" step="1">
											<c:set var="tempGrade" value="${6 - grade}"/>
											<c:if test="${processedReview.review.grade <= tempGrade}">
												<img style="float:left" class="grade_img" src="/static/img/star_on.png" width="15" height="15">
											</c:if>
											<c:if test="${processedReview.review.grade > tempGrade}">
												<img style="float:left" class="grade_img" src="/static/img/star_off.png" width="15" height="15">
											</c:if>
										</c:forEach>
									</div>
								</div>
										
								<div class="review_like_and_dislike">
									<table style="width:100%">
										<tr>
											<td>
												좋아요 <span class="point">${processedReview.review.like}</span>	
											</td>
											<td>
												싫어요 <span class="point">${processedReview.review.disLike}</span>
											</td>
											<td>
												공유하기
											</td>
										</tr>
									</table>
								</div>
							</c:if>
						</div><!-- add div -->
					</li>
				</c:forEach>
			</c:if>
		</ul>
	<c:if test="${empty processedReviewList}">
		<div id="noResult">결과가 존재하지 않습니다.</div>
	</c:if>
</div>
<%@include file="common/footer.jsp"%>
<script type="text/javascript">
$(".clickReviewNo").click(function (oEvent){
	_clickReviewNo(oEvent);
});

function _clickReviewNo(oEvent){
   var elCurrentTarget = oEvent.currentTarget;
   if (!elCurrentTarget) {
      oEvent.stopPropagation();
      return;
   }
   
   var params = parseInt(extractParams(elCurrentTarget.className));
   location.href = "showReview.do?reviewNo=" + params;
}

function extractParams(sClassName) {
	   return sClassName.replace(/.*_params\('([^\)]+)'\).*/g, '$1').split(',');
};
</script>