package com.crew91.beautiwiki.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import org.apache.ibatis.session.SqlSession;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.crew91.beautiwiki.vo.BrandVO;
import com.crew91.beautiwiki.vo.PreferVO;
import com.crew91.beautiwiki.vo.UserVO;
@Component
public class PreferDAO extends SqlSessionDaoSupport  {
	@Autowired
    private SqlSession sqlSession;
	@Autowired
	private PreferVO prefervo;
	@Autowired
	private DataSource ds;
	public PreferDAO() throws Exception {
		Context init = new InitialContext();
		ds = (DataSource)init.lookup("java:comp/env/jdbc/OracleDB");
	}
	
	public int insertPrefer(List<PreferVO> preferVOlist) throws SQLException{
		int ox = 0;
		for (PreferVO preferVO : preferVOlist) {
			ox = getSqlSession().insert("tb_prefer.insertPreferInfo", preferVO);
			if (ox == 0) {
				break;
			}
		}
		return ox;
	}
	public boolean insertPrefer(int userNo, int[] prefer) throws SQLException{//회원수정시 userNo를 가지고 새로운 prefer을 추가한다.
		Connection conn = null;
		PreparedStatement pstmt = null;
		String sql;
		
		try {
			for(int i : prefer) {
				sql = "insert into tb_prefer values (sq_prefer.nextval, ?, ?)";
				conn = ds.getConnection();
				pstmt = conn.prepareStatement(sql);
				pstmt.setInt(1, userNo);
				pstmt.setInt(2, i);
				pstmt.executeUpdate();
			}	
		}catch(Exception e) {
			e.printStackTrace();
			return false;
			
		}finally {
			if(pstmt != null) {
				pstmt.close();
			}
			if(conn != null) {
				conn.close();
			}
		}
		return true;
	}
	
	
	public List<BrandVO> getPrefer(int userNo) throws SQLException{//회원정보중 회원의 선호제품을 얻어온다.
		Connection conn = null;
		PreparedStatement pstmt = null;
		List<BrandVO> alist = new ArrayList<BrandVO>();
	
		String sql = "SELECT name FROM tb_brand where brand_id in (select brand_id from tb_prefer where user_id = ?)";
		try {
			conn = ds.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, userNo);
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				BrandVO brand = new BrandVO();
				brand.setName(rs.getString("name"));

				alist.add(brand);
			}
					
		}catch(Exception e) {
			e.printStackTrace();
			return null;
			
		}finally {
			if(pstmt != null) {
				pstmt.close();
			}
			if(conn != null) {
				conn.close();
			}
		}
		return alist;
	}
	
	public boolean deletePrefer(int user_no) throws SQLException{//탈퇴시 회원의 모든 prefer을 삭제한다.
		Connection conn = null;
		PreparedStatement pstmt = null;

		String sql = "delete FROM tb_prefer where user_id=" + user_no;
				
		try {
			conn = ds.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.executeUpdate(sql);

			
		}catch(Exception e) {
			e.printStackTrace();
			
		}finally {
			if(pstmt != null) {
				pstmt.close();
			}
			if(conn != null) {
				conn.close();
			}
		}
		return true;
	}

	public boolean deleteUserPrefer(int userNo, int[] prefer) throws SQLException{//수정시 user_no로 갖고 삭제를한다.
		Connection conn = null;
		PreparedStatement pstmt = null;
		String sql = "delete from tb_prefer where user_id =" + userNo;
		
		try {
			conn = ds.getConnection();
			pstmt = conn.prepareStatement(sql);
			pstmt.executeUpdate();
			
		}catch(Exception e) {
			e.printStackTrace();
			return false;
			
		}finally {
			if(pstmt != null) {
				pstmt.close();
			}
			if(conn != null) {
				conn.close();
			}
		}
		return true;
	}
	

}
