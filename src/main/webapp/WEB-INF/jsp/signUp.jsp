<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!doctype html>
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<title>회원가입</title>
</head>
<body>
<c:set var="currentPageCss" value="main.css" scope="request" />
<jsp:include page="/WEB-INF/jsp/common/header.jsp" />

<section id = "container">
	<div class="wrap" text-align = center style="width:30%; margin:0 auto;">
	<br><br>
	<h2><font color = #DB4A37>회원가입</font></h2>
		<p>&nbsp;&nbsp;&nbsp;<br></p>
		<form id="f" action = "signUpService.do" method="post">
			<p>	
				<input type = "text" class="input_row" size = 40 name = "userId" placeholder="username or e-mail"/>
			</p>
			<p>
				<input  class="input_row" type = "password" size = 40 name = "password" placeholder="password"/>
			</p>
			<p>
				<input type = "password" class="input_row" size = 40 name = "password2" placeholder="confirm password"/>
			</p>
			<p>
				<input type = "text" class="input_row" size = 40 name = "userName" placeholder="name"/>
			</p>
			<p>
				<input type = "text" size = 30 name = "age"  class="input_row" placeholder="age"/>
			</p>
			<p align = left>
				<font color=gray>성별</font> &nbsp;&nbsp;
				<select name = "sex">
					<option value = "0">남성</option>
					<option value = "1">여성</option>
				</select>
			</p>
			<p align = left>
				<font color=gray>피부타입</font>&nbsp;&nbsp;
				<select name = "skinType"> 
					<option value = "건성">건성</option>
					<option value = "지성">지성</option>
					<option value = "중성">중성</option>
				</select>
			</p>
			<p align = left>
				<font color=gray>선호회사 제품</font><br>
				<c:forEach items="${allBrandList}" var="brand" begin="0" end="3">
					<input type="checkbox" name="brandName" value="${brand.brandId}">${brand.name}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	
				</c:forEach>
				<br>
				<c:forEach items="${allBrandList}" var="brand" begin="4" end="7">
					<input type="checkbox" name="brandName" value="${brand.brandId}">${brand.name}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				</c:forEach>
				<br>
				<c:forEach items="${allBrandList}" var="brand" begin="8" end= "11">
					<input type="checkbox" name="brandName" value="${brand.brandId}">${brand.name}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				</c:forEach>
				<br>
				<c:forEach items="${allBrandList}" var="brand" begin="12" end= "15">
					<input type="checkbox" name="brandName" value="${brand.brandId}">${brand.name}&nbsp;
				</c:forEach>
			</p>
			<p class="input_btn_area">
				<input type="submit" class="input_btn" id="signUp" value="SIGNUP"/>
			</p>
			<p class="input_btn_area">
				<input type="button" class="input_btn" id="back" value="취소" onClick="history.go(-1)"/>
			</p>
		</form>
	</div>
</section>
<jsp:include page="/WEB-INF/jsp/common/footer.jsp" />
</body>
</html>