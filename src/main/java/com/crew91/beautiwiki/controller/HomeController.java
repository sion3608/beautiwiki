/*
 * 작성자 : 김 시온
 * 일자 : 2014.11.17일
 * 목적 : Home
 * 수정사항 : 
 */
package com.crew91.beautiwiki.controller;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.impl.Log4JLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.crew91.beautiwiki.bo.MatchingBO;
import com.crew91.beautiwiki.bo.ReviewBO;
import com.crew91.beautiwiki.bo.SearchBO;
import com.crew91.beautiwiki.dao.BrandDAO;
import com.crew91.beautiwiki.dao.UserDAO;
import com.crew91.beautiwiki.vo.CosmeticVO;
import com.crew91.beautiwiki.vo.ReviewVO;
import com.crew91.beautiwiki.vo.UserVO;

@Controller
@SessionAttributes({"userNo", "userId"})
public class HomeController {
	public static final Log4JLogger LOG = new Log4JLogger("HomeController");
	
	@Autowired
	SearchBO searchBO;
	
	@Autowired
	UserDAO userDAO;
	
	@Autowired
	MatchingBO matchingBO;

	@Autowired
	ReviewBO reviewBO;
	
	@Autowired
	BrandDAO brandDAO;
	
	/**
	 * 2차 카테고리 목록을 가져온다.
	 * 
	 * @param model
	 * @param catId
	 * @return
	 */
	@RequestMapping(value="/home.do")
	public String index(Model model, @ModelAttribute("userId") String userId) {
		try {
			UserVO userVO = userDAO.getUserInfo(userId);
			
			List<ReviewVO> remainReviewList = matchingBO.requisiteReview(userVO.getUserNo(), userVO.getAge());
			List<CosmeticVO> resultMatching = matchingBO.matchingReview(remainReviewList); 
			//최신 리뷰 select해오는 문장
			List<Map<String, Object>> newestReviewList = reviewBO.newestReviewList();
			//hit수 많은 리뷰 select해오는 문장
			List<Map<String, Object>> highHitReviewList = reviewBO.highHitReviewList();
			for (CosmeticVO cosmeticVO : resultMatching) {
				cosmeticVO.setImgUrl("/editor/cosmetics/" + StringUtils.trimWhitespace(cosmeticVO.getName()) + ".JPG");
			}
			
			for (CosmeticVO cosmetic : resultMatching) {
				int brandId = cosmetic.getBrandId();
				String brandName = brandDAO.selectBrandNameByBrandId(brandId);
				cosmetic.setBrandName(brandName);
			}

			model.addAttribute("matchingList", resultMatching);
			model.addAttribute("newestReviewList", newestReviewList);
			model.addAttribute("highHitReviewList", highHitReviewList);
			
			// 네비게이션에 필요한 정보 조회
			model.addAttribute("navigationInfo", searchBO.getNavigationInfo());
		} catch (SQLException e) {
		}

		return "home";
	}
}
