package com.crew91.beautiwiki.vo;

import org.springframework.stereotype.Component;

@Component
public class UserVO{
	private int userNo;
	private String userId;
	private String pw;
	private int sex;
	private int age;
	private String skinType;
	private String userName;
	
	public UserVO() {}
	public UserVO(int userNo, int sex, int age, String skinType) {
		this.userNo = userNo;
		this.sex = sex;
		this.age = age;
		this.skinType = skinType;
	}
	public UserVO(int userNo, int age) {
		this.userNo = userNo;
		this.age = age;
	}
	public UserVO(String userId, String pw, int sex, int age, String skinType, String userName) {
		this.userId = userId;
		this.pw = pw;
		this.sex = sex;
		this.age = age;
		this.skinType = skinType;
		this.userName = userName;
	}
	public UserVO(int userNo, String userId, String pw, int sex, int age, String skinType, String userName) {
		this.userNo = userNo;
		this.userId = userId;
		this.pw = pw;
		this.sex = sex;
		this.age = age;
		this.skinType = skinType;
		this.userName = userName;
	}
	
	public int getUserNo() {
		return userNo;
	}
	public String getUserId() {
		return userId;
	}
	public String getPw() {
		return pw;
	}
	public int getSex() {
		return sex;
	}
	public int getAge() {
		return age;
	}
	public String getSkinType() {
		return skinType;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserNo(int userNo) {
		this.userNo = userNo;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public void setPw(String pw) {
		this.pw = pw;
	}
	public void setSex(int sex) {
		this.sex = sex;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public void setSkinType(String skinType) {
		this.skinType = skinType;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Override
	public String toString() {
		return "UserVO [userNo=" + userNo + ", userId=" + userId + ", pw=" + pw
				+ ", sex=" + sex + ", age=" + age + ", skinType=" + skinType
				+ ", userName=" + userName + "]";
	}

}
