<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="EUC-KR"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<script language = "javascript">
function updateUser(){
	 
	document.f.action = "updateUser2.do";
	f.submit();
}
</script>

<c:set var="currentPageCss" value="main.css" scope="request" />
<jsp:include page="/WEB-INF/jsp/common/header.jsp" />

<section id = "container">
	<div class="wrap" text-align = center style="width:30%; margin:0 auto;">
		<br><br>
		<h2><font color = #DB4A37>회원정보수정</font></h2>
		<p>&nbsp;&nbsp;&nbsp;<br></p>
		<form name = f method = post> <%--폼안의 모든 정보를 컨트롤러에 보낸다. --%>
			<input type = hidden name = "userNo" value = "${userInfo.userNo}">
			<input type = hidden name = "userId" value = "${userInfo.userId}">
		<p> 
			<table border = 0 width = 500>
				<tr height = 50>
					<td><font color = grey>아이디</font></td>
					<td>${userInfo.userId}</td>
				</tr>
				<tr height = 50>
					<td><font color = grey>비밀번호</font></td>
					<td><input type = text class="input_row" name = password value= "${userInfo.pw}"></td> 
				</tr>
				<tr height = 50>
					<td><font color = grey>이름</font>
					<td><input type = text class="input_row" name = userName value = "${userInfo.userName}"></td>
				</tr>
				<tr height = 50>
					<td><font color = grey>나이</font></td>
					<td><input type = text class="input_row" name = age value = "${userInfo.age}"></td>
				</tr>
				<tr height = 50>
					<td><font color = grey>성별</font></td>
					<td>
						<select name = "sex">
						<c:if test="${userInfo.sex eq '0'}">
				    		<option value = "0" selected>남성</option>
							<option value = "1">여성</option>
				    	</c:if>
				    	<c:if test="${userInfo.sex eq '1' }">
				    		<option value = "0">남성</option>
							<option value = "1" selected>여성</option>
				    	</c:if>
				    	</select>
					</td>
				</tr>
				<tr height = 50>
					<td><font color = grey>스킨타입</font></td>
					<td>
						<select name = "skinType"> 
							<c:if test = "${userInfo.skinType eq '건성'}">
								<option value = "건성" selected>건성</option>
								<option value = "지성">지성</option>
								<option value = "중성">중성</option>
							</c:if>
							<c:if test = "${userInfo.skinType eq '지성'}">
								<option value = "건성">건성</option>
								<option value = "지성" selected>지성</option>
								<option value = "중성">중성</option>
							</c:if>
							<c:if test = "${userInfo.skinType eq '중성'}">
								<option value = "건성">건성</option>
								<option value = "지성">지성</option>
								<option value = "중성" selected>중성</option>
							</c:if>
						</select>
					</td>
				</tr>
				<tr height = 50>
					<td><font color = grey>선호회사제품</font></td>
					<td>
						<c:set var = "t_idx" value = "0"/>
						<c:forEach items="${brandList}" var="brand" varStatus = "status">
							<c:choose>
						    <c:when test = "${userPrefer[t_idx].brandId + 1 eq brand.brandId}">
				    			<input type = checkbox name = prefer value=${brand.brandId} checked>${brand.name}
					    	</c:when>
							<c:otherwise>
					       		<input type = checkbox name = prefer value=${brand.brandId}>${brand.name}
					        </c:otherwise>
							</c:choose>
							<c:if test = "${status.count%4 eq 0}"><br></c:if>
						</c:forEach>
					</td>
				</tr>
			</table>
		</p>
		<p class="input_btn_area">
			<input type = button  class="input_btn"  value = "수정하기" onClick = "updateUser()">
		</p>
		<p class="input_btn_area">
			<input type = button  class="input_btn" value = "취소하기" onClick = "history.go(-1)">
		</p>
	</form>
	</div>
</section>
<jsp:include page="/WEB-INF/jsp/common/footer.jsp" />
</body>
</html>