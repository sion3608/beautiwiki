package com.crew91.beautiwiki.dao;

import java.sql.SQLException;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.crew91.beautiwiki.vo.BrandVO;

@Component
public class BrandDAO extends SqlSessionDaoSupport{
	@Autowired
    private SqlSession sqlSession;
	
	public  List<BrandVO> selectAllBrand() throws SQLException {
		
		return getSqlSession().selectList("tb_brand.selectAllBrand");//tb_brand.xml, //tb_brand.xml, 파라미터 1
		
	}
	
	public String selectBrandNameByBrandId(int brandId) throws SQLException {
		return getSqlSession().selectOne("selectBrandNameByBrandId", brandId);
	}
}
