<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!doctype html>
<html lang="ko">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta property="fb:app_id" content="APP_ID" />
<meta property="og:type" content="website" />
<meta property="og:title" content="웹 페이지 제목" />
<meta property="og:url" content="웹 페이지 URL" />
<meta property="og:description" content="웹 페이지 내용" />
<meta property="og:image" content="웹 페이지 대표 이미지" />
<title>${title}</title>
<c:set var="JS_DIR" value="static/js" scope="request"/>
<c:set var="CSS_DIR" value="static/css" scope="request"/>
<c:set var="IMG_DIR" value="static/img" scope="request"/>
<link rel="stylesheet" href="${CSS_DIR}/common.css" />
<c:if test="${not empty currentPageCss}">
	<c:forTokens items="${currentPageCss}" delims="," var="css">	
		<link rel="stylesheet" type="text/css" href="${CSS_DIR}/${fn:trim(css)}" />
	</c:forTokens>
</c:if>
<script type="text/javascript" src="${JS_DIR}/jindo.desktop.ns.js"></script>
<script type="text/javascript" src="${JS_DIR}/jindo_component.js"></script>
<script type="text/javascript" src="${JS_DIR}/jindo_mobile_component.js"></script>
<script type="text/javascript" src="${JS_DIR}/jquery-1.10.1.js"></script>
</head>
<body>
<div class="nav _navigationContainer">
	<div class="nav_1 _navigationBar">
		<div class="_bottomLine" style="position:absolute;height:2px;top:78px;width:100%;background-color:#BC4036;z-index:2000"></div>
		<div class="nav_1_padding">
			<div class="nav_left no_padding">
				<div class="nav_1_logo_area" >
					<a href="home.do" style="text-decoration:none">
						<div class="nav_1_logo" >
							<span style="float:left">beautiwiki</span><img style="float:left;margin-top:-28px;" src="/static/img/leaf.png" height="60px" width="60px">
						</div>
					</a>
				</div>
			</div>
			
			<div class="nav_right no_padding">
				<div class="nav_1_logout_area">
					<input type="button" class="nav_1_logout" name="navLogout" value="" onclick="javascript:location.href='/logout.do'">
				</div>
			</div>
			
			<div class="nav_right no_padding">
				<div class="nav_1_setting_area">
					<input type="button" class="nav_1_setting" name="navSetting" value="" onclick="javascript:location.href='/updateUser1.do?upNo=1&userId=${userId}'">
				</div>
			</div>
			
			<div class="nav_right no_padding">
				<div class="nav_1_cart_area">
					<input type="button" class="nav_1_cart" name="navCart" value="" onclick="javascript:location.href='/showMyReview.do'">
				</div>
			</div>
			
			<div class="nav_right no_padding">
				<div class="nav_1_write_area">
					<input type="button" class="nav_1_write" name="navCreateReview" value="" onclick="javascript:location.href='/createReview.do'">
				</div>
			</div>
			
			<div class="nav_right no_padding">
				<div class="nav_1_search_btn_area">
					<input type="button" class="nav_1_search_btn" name="navSearchBtn" value="">
				</div>
			</div>
			
			<div class="nav_right" style="padding-right:10px;">
				<div class="nav_1_search_input_area">
					<input type="search" id="searchKeyword" class="nav_1_search_input" name="navSearchInput" value="" placeholder="검색">
				</div>
			</div>
		</div>
	</div>
	
	<div class="nav_2 _subNavigationBar">
		<div class="_bottomLine" style="position:absolute;bottom:0;height:1px;width:100%;background-color:#C7C0BA;"></div>
		<div class="nav_2_padding">
			<c:if test="${not empty navigationInfo && not empty navigationInfo.categoryInfoList}">
				<c:forEach var="categoryInfo" items="${navigationInfo.categoryInfoList}" varStatus="sequence">
					<div class="nav_left<c:if test="${sequence.index eq 0}"> no_padding</c:if>">
						<div class="nav_2_category _category _params('${categoryInfo.categoryId},0')">
							${categoryInfo.categoryName}
							<div class="nav_2_sub_category_area _subcategoryArea" style="display:none">
								<c:if test="${not empty categoryInfo.subCategoryList}">
									<c:forEach var="subCategoryInfo" items="${categoryInfo.subCategoryList}" varStatus="sequence">
										<div class="nav_2_sub_category _subcategory _params('${categoryInfo.categoryId},${subCategoryInfo.subcategoryId}')">
											${subCategoryInfo.name}
										</div>
									</c:forEach>
								</c:if>
							</div>
						</div>
					</div>	
				</c:forEach>
			</c:if>
			<c:if test="${not empty navigationInfo && not empty navigationInfo.brandList}">
				<div class="nav_left">
					<div class="nav_2_category _category _params('0,0')">
						<span>브랜드</span>
						<div class="nav_2_sub_category_area _subcategoryArea" style="display:none">
							<c:forEach var="brand" items="${navigationInfo.brandList}" varStatus="sequence">
								<div class="nav_2_sub_category _subcategory _params('0,${brand.brandId}')">
									${brand.name}
								</div>
							</c:forEach>
						</div>
					</div>
				</div>
			</c:if>
		</div>
	</div>
</div>

<script type="text/javascript">
$(".nav_1_search_btn").click(function (oEvent){
	_searchKeyword(oEvent);
});

function _searchKeyword(oEvent){
	var elKeyword = $("#searchKeyword").val();
	console.log(elKeyword);
	location.href = "searchKeywordForReview.do?searchKeyword=" + elKeyword;
}
</script>
<div class="content">